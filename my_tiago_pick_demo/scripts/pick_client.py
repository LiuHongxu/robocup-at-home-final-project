#!/usr/bin/env python

# Copyright (c) 2016 PAL Robotics SL. All Rights Reserved
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# Author:
#   * Sam Pfeiffer
#   * Job van Dieten
#   * Jordi Pages

import rospy
import time
from tiago_pick_demo.msg import PickUpPoseAction, PickUpPoseGoal
from geometry_msgs.msg import PoseStamped, Pose
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from play_motion_msgs.msg import PlayMotionAction, PlayMotionGoal
from actionlib import SimpleActionClient

import tf2_ros
from tf2_geometry_msgs import do_transform_pose

import numpy as np
from std_srvs.srv import Empty

import cv2
from cv_bridge import CvBridge

from moveit_msgs.msg import MoveItErrorCodes
moveit_error_dict = {}
for name in MoveItErrorCodes.__dict__.keys():
	if not name[:1] == '_':
		code = MoveItErrorCodes.__dict__[name]
		moveit_error_dict[code] = name

class SphericalService(object):
	def __init__(self):
		rospy.loginfo("Starting Spherical Grab Service")
#		self.pick_type = PickAruco()
		rospy.loginfo("Finished SphericalService constructor")
#                self.place_gui = rospy.Service("/place_gui", Empty, self.start_aruco_place)
#                self.pick_gui = rospy.Service("/pick_gui", Empty, self.start_aruco_pick)

                self.pre_grasp_handle = rospy.Service("/pre_grasp_handle", Empty, self.pre_grasp_robot)
                self.pre_grasp_object = rospy.Service("/pre_grasp_object", Empty, self.pre_grasp_object)
                self.pre_grasp_object1 = rospy.Service("/pre_grasp_object1", Empty, self.pre_grasp_object1)
                self.pre_grasp_shake = rospy.Service("/pre_grasp_shake", Empty, self.pre_grasp_shake)
                self.home = rospy.Service("/home", Empty, self.home)
                self.down_head = rospy.Service("/down_head", Empty, self.lower_head)
                self.gripper_off = rospy.Service("/gripper_off", Empty, self.gripper_off)
		self.torso_cmd = rospy.Publisher(
			'/torso_controller/command', JointTrajectory, queue_size=1)
		self.arm_cmd = rospy.Publisher(
			'/arm_controller/command', JointTrajectory, queue_size=1)
		self.head_cmd = rospy.Publisher(
			'/head_controller/command', JointTrajectory, queue_size=1)
		self.gripper_cmd = rospy.Publisher(
			'/gripper_controller/command', JointTrajectory, queue_size=1)
		self.play_m_as = SimpleActionClient('/play_motion', PlayMotionAction)

	def start_aruco_pick(self, req):
		self.pick_type.pick_aruco("pick")
		return {}

	def start_aruco_place(self, req):
		self.pick_type.pick_aruco("place")
		return {}

	def lower_head(self, req):        
		rospy.loginfo("Moving head down")
		jt = JointTrajectory()
		jt.joint_names = ['head_1_joint', 'head_2_joint']
		jtp = JointTrajectoryPoint()
		jtp.positions = [0.0, -0.44]
		jtp.time_from_start = rospy.Duration(2.0)
		jt.points.append(jtp)
		self.head_cmd.publish(jt)
                rospy.loginfo("Done.")
		return {}
	def gripper_off(self, req):        
		rospy.loginfo("gripper off")
		jt = JointTrajectory()
		jt.joint_names = ['gripper_left_finger_joint,', 'gripper_right_finger_joint']
		jtp = JointTrajectoryPoint()
		jtp.positions = [0.04, 0.04]
		jtp.time_from_start = rospy.Duration(5.0)
		jt.points.append(jtp)
		self.gripper_cmd.publish(jt)
                rospy.loginfo("Done.")
		return {}
	def home(self, req):
		rospy.loginfo("move to home")
		pmg = PlayMotionGoal()
		pmg.motion_name = 'home'
		pmg.skip_planning = False
		self.play_m_as.send_goal_and_wait(pmg)
	def pre_grasp_robot(self, req):
		rospy.loginfo("pregrasp to catch the handle")
		pmg = PlayMotionGoal()
		pmg.motion_name = 'offer_gripper'
		pmg.skip_planning = False
		self.play_m_as.send_goal_and_wait(pmg)
                self.down_torso()
		jt = JointTrajectory()
		jt.joint_names = ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint']
		jtp = JointTrajectoryPoint()
		jtp.positions = [1.65523, -1.07768, -3.16786, 2.2323, -1.603307, -1.09423, -1.58677]
		jtp.time_from_start = rospy.Duration(4.0)
		jt.points.append(jtp)
		self.arm_cmd.publish(jt)
		rospy.loginfo("Robot pre-grasped for opening the door.")
		return {}

        def down_torso(self):
		rospy.loginfo("Moving torso down")
		jt = JointTrajectory()
		jt.joint_names = ['torso_lift_joint']
		jtp = JointTrajectoryPoint()
                jtp.positions = [0.0498]
		jtp.time_from_start = rospy.Duration(2.5)
		jt.points.append(jtp)
		self.torso_cmd.publish(jt)

	def pre_grasp_object(self, req):
		rospy.loginfo("Unfold arm safely")
		pmg = PlayMotionGoal()
		pmg.motion_name = 'pregrasp'
		pmg.skip_planning = False
		self.play_m_as.send_goal_and_wait(pmg)
                self.up_torso() 
		jt = JointTrajectory()
		jt.joint_names = ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint']
		jtp = JointTrajectoryPoint()
		jtp.positions = [0.7499, 0.6637, -1.36, 1.87, 0.5379, -1.19, 0.8725]
		jtp.time_from_start = rospy.Duration(4.0)
		jt.points.append(jtp)
		self.arm_cmd.publish(jt)
		rospy.loginfo("Robot pre-grasped for grasping the object.")
		return {}

        def up_torso(self):
		rospy.loginfo("Moving torso up")
		jt = JointTrajectory()
		jt.joint_names = ['torso_lift_joint']
		jtp = JointTrajectoryPoint()
                jtp.positions = [0.3496]
		jtp.time_from_start = rospy.Duration(2.5)
		jt.points.append(jtp)
		self.torso_cmd.publish(jt)

	def pre_grasp_object1(self, req):
                self.up_torso1()
		rospy.loginfo("Moving torsor up and head down")
		jt = JointTrajectory()
		jt.joint_names = ['head_1_joint', 'head_2_joint']
		jtp = JointTrajectoryPoint()
		jtp.positions = [0.0, -0.83]
		jtp.time_from_start = rospy.Duration(2.0)
		jt.points.append(jtp)
		self.head_cmd.publish(jt)
		rospy.loginfo("Robot pre-grasped for grasping the object 1.")
		return {}

        def up_torso1(self):
		jt = JointTrajectory()
		jt.joint_names = ['torso_lift_joint']
		jtp = JointTrajectoryPoint()
                jtp.positions = [0.3496]
		jtp.time_from_start = rospy.Duration(2.5)
		jt.points.append(jtp)
		self.torso_cmd.publish(jt)
	def pre_grasp_shake(self, req):
		rospy.loginfo("pregrasp for hands shaking")
                self.up_torso2()
		jt = JointTrajectory()
		jt.joint_names = ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint']
		jtp = JointTrajectoryPoint()
		jtp.positions = [1.60992, -0.93013, -3.13995, 1.830024, -1.57693, -0.8432, -1.577]
		jtp.time_from_start = rospy.Duration(4.0)
		jt.points.append(jtp)
		self.arm_cmd.publish(jt)
		rospy.loginfo("Robot pre-grasped for shaking hands.")
		return {}

        def up_torso2(self):
		rospy.loginfo("Moving torso up")
		jt = JointTrajectory()
		jt.joint_names = ['torso_lift_joint']
		jtp = JointTrajectoryPoint()
                jtp.positions = [0.3]
		jtp.time_from_start = rospy.Duration(2.5)
		jt.points.append(jtp)
		self.torso_cmd.publish(jt)


class PickAruco(object):
	def __init__(self):
		rospy.loginfo("Initalizing...")
                self.bridge = CvBridge()
		self.tfBuffer = tf2_ros.Buffer()
                self.tf_l = tf2_ros.TransformListener(self.tfBuffer)
                
		rospy.loginfo("Waiting for /pickup_pose AS...")
		self.pick_as = SimpleActionClient('/pickup_pose', PickUpPoseAction)
                time.sleep(1.0)
		if not self.pick_as.wait_for_server(rospy.Duration(20)):
			rospy.logerr("Could not connect to /pickup_pose AS")
			exit()
		rospy.loginfo("Waiting for /place_pose AS...")
		self.place_as = SimpleActionClient('/place_pose', PickUpPoseAction)

		self.place_as.wait_for_server()

		rospy.loginfo("Setting publishers to torso and head controller...")
		self.torso_cmd = rospy.Publisher(
			'/torso_controller/command', JointTrajectory, queue_size=1)
		self.head_cmd = rospy.Publisher(
			'/head_controller/command', JointTrajectory, queue_size=1)
		self.detected_pose_pub = rospy.Publisher('/detected_aruco_pose',
							 PoseStamped,
							 queue_size=1,
							 latch=True)

		rospy.loginfo("Waiting for '/play_motion' AS...")
		self.play_m_as = SimpleActionClient('/play_motion', PlayMotionAction)
		if not self.play_m_as.wait_for_server(rospy.Duration(20)):
			rospy.logerr("Could not connect to /play_motion AS")
			exit()
		rospy.loginfo("Connected!")
		rospy.sleep(1.0)
		rospy.loginfo("Done initializing PickAruco.")

   	def strip_leading_slash(self, s):
		return s[1:] if s.startswith("/") else s
		
	def pick_aruco(self, string_operation):

                if string_operation == "pick":
                        self.prepare_robot()
                        rospy.sleep(5.0)

                        rospy.loginfo("spherical_grasp_gui: Waiting for an object detection")

                        aruco_pose = rospy.wait_for_message('/3D_point', PoseStamped)
                        aruco_pose.header.frame_id = self.strip_leading_slash(aruco_pose.header.frame_id)
                        rospy.loginfo("Got: " + str(aruco_pose))


                        rospy.loginfo("spherical_grasp_gui: Transforming from frame: " +
                        aruco_pose.header.frame_id + " to 'base_footprint'")
                        ps = PoseStamped()
                        ps.pose.position = aruco_pose.pose.position
                        ps.header.stamp = self.tfBuffer.get_latest_common_time("base_footprint", aruco_pose.header.frame_id)
                        ps.header.frame_id = aruco_pose.header.frame_id
                        transform_ok = False
                        while not transform_ok and not rospy.is_shutdown():
                                try:
                                        transform = self.tfBuffer.lookup_transform("base_footprint",
                                                                                   ps.header.frame_id,
                                                                                   rospy.Time(0))
                                        aruco_ps = do_transform_pose(ps, transform)
                                        transform_ok = True
                                except tf2_ros.ExtrapolationException as e:
                                        rospy.logwarn(
                                                "Exception on transforming point... trying again \n(" +
                                                str(e) + ")")
                                        rospy.sleep(0.01)
                                        ps.header.stamp = self.tfBuffer.get_latest_common_time("base_footprint", aruco_pose.header.frame_id)
                                pick_g = PickUpPoseGoal()

		if string_operation == "pick":

                        rospy.loginfo("Setting cube pose based on ArUco detection")
			pick_g.object_pose.pose.position = aruco_pose.pose.position
                        pick_g.object_pose.pose.position.z += 0.09
                        pick_g.object_pose.pose.position.x += 0.025

                        rospy.loginfo("aruco pose in base_footprint:" + str(pick_g))

			pick_g.object_pose.header.frame_id = 'base_footprint'
			pick_g.object_pose.pose.orientation.w = 1.0
			self.detected_pose_pub.publish(pick_g.object_pose)
			rospy.loginfo("Gonna pick:" + str(pick_g))
			self.pick_as.send_goal_and_wait(pick_g)
			rospy.loginfo("Done!")

			result = self.pick_as.get_result()
			while str(moveit_error_dict[result.error_code]) != "SUCCESS":
				rospy.logerr("Failed to pick, not trying further")
				rospy.loginfo("Try grasping again!")
				self.pick_as.send_goal_and_wait(pick_g)
				result = self.pick_as.get_result()
				return

			# Move torso to its maximum height
                        self.lift_torso()

                        # Raise arm to a safe pose
			rospy.loginfo("Moving arm to a safe pose")
			pmg = PlayMotionGoal()
                        pmg.motion_name = 'pick_final_pose'
			pmg.skip_planning = False
			self.play_m_as.send_goal_and_wait(pmg)
			rospy.loginfo("Raise object done.")

                        rospy.sleep(3.0)

                        # Place the object back to its position
			rospy.loginfo("Go back home!")
			pmg = PlayMotionGoal()
                        pmg.motion_name = 'home'
			pmg.skip_planning = False
			self.play_m_as.send_goal_and_wait(pmg)
			rospy.loginfo("Go back home done.")

                        rospy.sleep(3.0)
			rospy.loginfo("Done!")

		if string_operation == "place":

			#aruco_pose = PoseStamped()
                        #aruco_pose.header.frame_id = 'xtion_rgb_optical_fram'
                        #aruco_pose.header.frame_id = 'base_footprint'
                        #aruco_pose.pose.position.x = -1.8
                        #aruco_pose.pose.position.y = -0.3
                        #aruco_pose.pose.position.z = 0.962

			#rospy.loginfo("spherical_grasp_gui: Transforming from frame: " +
			#aruco_pose.header.frame_id + " to 'base_footprint'")
			#ps = PoseStamped()
			#ps.pose.position = aruco_pose.pose.position
			#ps.header.stamp = self.tfBuffer.get_latest_common_time("base_footprint", aruco_pose.header.frame_id)
			#ps.header.frame_id = aruco_pose.header.frame_id
			#transform_ok = False
			#while not transform_ok and not rospy.is_shutdown():
			#	try:
			#		transform = self.tfBuffer.lookup_transform("base_footprint", 
			#							   ps.header.frame_id,
			#							   rospy.Time(0))
			#		aruco_ps = do_transform_pose(ps, transform)
			#		transform_ok = True
			#	except tf2_ros.ExtrapolationException as e:
			#		rospy.logwarn(
			#			"Exception on transforming point... trying again \n(" +
			#			str(e) + ")")
			#		rospy.sleep(0.01)
			#		ps.header.stamp = self.tfBuffer.get_latest_common_time("base_footprint", aruco_pose.header.frame_id)
			#	pick_g = PickUpPoseGoal()

			#rospy.loginfo("Setting cube pose based on ArUco detection")
			pick_g = PickUpPoseGoal()
                        pick_g.object_pose.header.frame_id = 'map'
                        pick_g.object_pose.pose.position.x = 0
			pick_g.object_pose.pose.position.y = 0
			pick_g.object_pose.pose.position.z = 0
                        pick_g.object_pose.pose.orientation.w = 1.0

			rospy.loginfo("Clear Octomap before Placement!")
			
			self.place_as.send_goal_and_wait(pick_g)

			rospy.sleep(3.0)
			rospy.loginfo("Clear done!")


        def lift_torso(self):
		rospy.loginfo("Moving torso up")
		jt = JointTrajectory()
		jt.joint_names = ['torso_lift_joint']
		jtp = JointTrajectoryPoint()
                jtp.positions = [0.3]
		jtp.time_from_start = rospy.Duration(2.5)
		jt.points.append(jtp)
		self.torso_cmd.publish(jt)

        def down_torso(self):
		rospy.loginfo("Moving torso down")
		jt = JointTrajectory()
		jt.joint_names = ['torso_lift_joint']
		jtp = JointTrajectoryPoint()
                jtp.positions = [0.2]
		jtp.time_from_start = rospy.Duration(2.5)
		jt.points.append(jtp)
		self.torso_cmd.publish(jt)

        def lower_head(self):
		rospy.loginfo("Moving head down")
		jt = JointTrajectory()
		jt.joint_names = ['head_1_joint', 'head_2_joint']
		jtp = JointTrajectoryPoint()
		jtp.positions = [0.0, -0.66]
		jtp.time_from_start = rospy.Duration(2.0)
		jt.points.append(jtp)
		self.head_cmd.publish(jt)
                rospy.loginfo("Done.")

	def pre_grasp_robot((self, req)):
		rospy.loginfo("Unfold arm safely")
		pmg = PlayMotionGoal()
		pmg.motion_name = 'offer_gripper'
		pmg.skip_planning = False
		self.play_m_as.send_goal_and_wait(pmg)
		jt = JointTrajectory()
		jt.joint_names = ['torso_lift_joint', 'arm_1_joint', 'arm_3_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint']
		jtp = JointTrajectoryPoint()
		jtp.positions = [0.0, 1.66, -1.31, -3.14, 2.21, -1.58, -0.82, -1.58]
		jtp.time_from_start = rospy.Duration(4.0)
		jt.points.append(jtp)
		self.head_cmd.publish(jt)
		rospy.loginfo("Done.")

                self.lower_head()

		rospy.loginfo("Robot pre-grasped.")
		return {}

	def prepare_robot(self):
		rospy.loginfo("Unfold arm safely")
		pmg = PlayMotionGoal()
		pmg.motion_name = 'pregrasp'
		pmg.skip_planning = False
		self.play_m_as.send_goal_and_wait(pmg)

		rospy.loginfo("Done.")

                self.lower_head()

		rospy.loginfo("Robot prepared.")


if __name__ == '__main__':
	rospy.init_node('pick_aruco_demo')
	sphere = SphericalService()
	rospy.spin()

