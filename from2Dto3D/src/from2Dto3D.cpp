#include <ros/ros.h>
#include <ros/console.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/Char.h>
#include <vector>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <tf/transform_listener.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>


#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


// PCL specific includes
#include <pcl_ros/point_cloud.h> // enable pcl publishing
#include <sensor_msgs/PointCloud2.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
//#include <image_geometry/pinhole_camera_model.h>
#include <darknet_ros_msgs/BoundingBox.h>
#include <darknet_ros_msgs/BoundingBoxes.h>
//#include "perception_msgs/Rect.h"

// services
#include "from2dto3d/object.h"


using namespace std;
using namespace cv;


class From2Dto3D
{

    private:
      //! The node handle
      ros::NodeHandle nh_;
      //! Node handle in the private namespace
      ros::NodeHandle priv_nh_;

      //! Define publishers and subscribers
      ros::Publisher pub_object;
      ros::Subscriber sub_point, sub_box;
	  ros::ServiceServer srv_object_class;

      geometry_msgs::Point msg_object, msg_object_old;
      //! Define the pointcloud structure and the bounding box local copy

      int object_x, object_y;
      string object;
      Mat Image;
      pcl::PointCloud<pcl::PointXYZRGB> cloud;

      //! A tf transform listener if needed
      //tf::TransformListener listener_;


      //------------------ Callbacks -------------------

      //! Process clusters
      void processCloud(const sensor_msgs::PointCloud2ConstPtr& pc);
      //! Process bounding boxes
      //void processRect(const perception_msgs::RectConstPtr & r);

      void sub_yolo_callback(const darknet_ros_msgs::BoundingBoxesConstPtr& box);
      void sub_the_Image(const sensor_msgs::ImageConstPtr& msg);

	  bool srv_callback(from2dto3d::object::Request &req, from2dto3d::object::Response &res);

    public:
      //! Subscribes to and advertises topics
      From2Dto3D(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
      {

//        image_transport::ImageTransport it(nh_);
        image_transport::Subscriber sub_image;
        // subscribers to the bounding boxes and the point cloud
        // format:
        // sub_name = nh_.subscribe<Type>("topic", queuesize, Function_of_the_class, this);
        //ros::Duration(5).sleep();
        //ROS_INFO("Which object do you want to recognize ?");
        //cout<<"$:";
        //cin>>object;
        //ROS_INFO_STREAM("" << object);
        sub_point = nh_.subscribe<sensor_msgs::PointCloud2>("/xtion/depth_registered/points", 1, &From2Dto3D::processCloud, this);
        //sub_image = it.subscribe("/xtion/rgb/image_rect_color", 1, &From2Dto3D::sub_the_Image, this);// Publishers
        sub_box = nh_.subscribe("/darknet_ros/bounding_boxes", 1, &From2Dto3D::sub_yolo_callback, this);

        // format:
        //pub_name = nh_.advertise< Type >("topic", queuesize);
        pub_object = nh_.advertise<geometry_msgs::Point>("/segmentation/point3D", 1);

		srv_object_class = nh_.advertiseService("/object_class", &From2Dto3D::srv_callback, this);

        ROS_INFO("from2Dto3D initialized ...");

      }

      ~From2Dto3D() {}
};

void From2Dto3D::sub_the_Image(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_ptr;
  cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
  Image = cv_ptr->image;
}

void From2Dto3D::processCloud(const sensor_msgs::PointCloud2ConstPtr& pc)
{
    // store local data copy or shared, depending on the message
    pcl::fromROSMsg(*pc, cloud);
}

void From2Dto3D::sub_yolo_callback(const darknet_ros_msgs::BoundingBoxesConstPtr& box)
{
	if (object == "remote" || object == "apple")
	{

    if (cloud.isOrganized() != 1)
    {
        ROS_INFO("Cloud is unorganized !!");
    }
    else
    {
        int object_index ;
        float probablity= 0;
        for(int a = 0; a < box->bounding_boxes.size(); a++)
        {
           if(box->bounding_boxes[a].Class == object)
           {
               if (probablity == 0){
                   object_index = a;
                   probablity = box->bounding_boxes[a].probability;
               }
               else if(box->bounding_boxes[a].probability > probablity)
               {
                   object_index = a;
                   probablity = box->bounding_boxes[a].probability;
               }
           }
        }

        //calculate the center point of the bounding box and publish the 3D coordinate
        if(probablity > 0)
        {
            object_x = static_cast<int>( (box->bounding_boxes[object_index].xmin + box->bounding_boxes[object_index].xmax)/ 2);
            object_y = static_cast<int>( (box->bounding_boxes[object_index].ymin + box->bounding_boxes[object_index].ymax)/ 2);
            msg_object.x = cloud.at(object_x, object_y).x;
            msg_object.y = cloud.at(object_x, object_y).y;
            msg_object.z = cloud.at(object_x, object_y).z;
            //ROS_INFO_STREAM("Position of the " << object << " is x:" << msg_object.x << ", y:" << msg_object.y << ", z:" << msg_object.z);

            if (!isnan(msg_object.x) && !isnan(msg_object.y) && !isnan(msg_object.z)){
                msg_object_old = msg_object;
                pub_object.publish(msg_object);
            }
            else if(!isnan(msg_object_old.x)){
                pub_object.publish(msg_object_old);

            }
        }

    }

	}
}

bool From2Dto3D::srv_callback(from2dto3d::object::Request &req, from2dto3d::object::Response &res)
{
	if (req.object_class == "remote")
	{
		object = "remote";
		res.finish = true;
	}
	else if (req.object_class == "apple")
	{
		object = "apple";
		res.finish = true;
	}
	else
	{
		ROS_WARN_STREAM("No corresponding object class found!");
		res.finish = false;
	}

	return true;
}



int main(int argc, char** argv)
{
    ros::init(argc, argv, "from2dto3d");
    ros::NodeHandle nh;
    From2Dto3D node(nh);
    ros::spin();
//    ros::waitForShutdown();
    return 0;
}


