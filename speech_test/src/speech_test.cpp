#include <ros/ros.h>
#include <sound_play/sound_play.h>


void sleepok(int t, ros::NodeHandle &nh)
{
	if (nh.ok())
		sleep(t);
}


int main(int argc, char** argv)
{
	ros::init(argc, argv, "speech_node");
	ros::NodeHandle nh;

	sound_play::SoundClient sc;

	sleepok(1, nh);

	sc.say("Hello I am TIAGO, nice to meet you.");

	sleepok(2, nh);
	
	ros::spin();

}
