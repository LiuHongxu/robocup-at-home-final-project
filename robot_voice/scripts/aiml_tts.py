#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy, os, sys
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient
from std_msgs.msg import String
import time
from robot_voice.srv import *

response = ''
previous_word = '?'
gflag = True
rospy.init_node('aiml_tts', anonymous = True)
#  pub = rospy.Publisher('result', String, queue_size=10)
r = rospy.Rate(1)
soundhandle = SoundClient()
rospy.sleep(1)
soundhandle.stopAll()
print 'Starting TTS'

# 获取应答字符，并且通过语音输出
def get_response(data):

    #time.sleep(5)

    global gflag
    global response
    if gflag:
        if data.data == 'apple':
            response = data.data
            #  soundhandle.say('ok, I will grasp apple.')
        #elif data.data == 'banana':
        #    response = data.data
        #soundhandle.say('ok, I will grasp banana.')
        elif data.data == 'remote control':
            response = 'remote_control'
            soundhandle.say('ok, I will grasp remote control.')
        elif data.data == 'control':
            response = 'remote_control'
            soundhandle.say('ok, I will grasp remote control.')
        elif data.data == 'remote':
            response = 'remote_control'
            soundhandle.say('ok, I will grasp remote control.')

        rospy.loginfo("Response: %s",response)
    #pub.publish(response)

    #  data.data = 'lalala'
    #  response = 'lalala'


def get_object_class(req):
    soundhandle.say('I find remote control and apple in the drawer. What do you want me to grasp?')
    time.sleep(11)
    global response, previous_word
    #  previous_word = ''
    #  while response != 'remote_control' and response != 'apple':
    while response != 'remote_control':
        rospy.loginfo("Response: %s",response)
        if(response != '' and previous_word != response):
            previous_word = response
            soundhandle.say('Sorry, I cannot understand.')
            time.sleep(20)
        #rospy.spinOnce()
    global gflag
    gflag = False
    return ObjectClassResponse(response)

# 订阅语音识别后的应答字符
def listener():
    rospy.loginfo("Starting listening to response")
    s = rospy.Service('object_voice', ObjectClass, get_object_class)
    rospy.Subscriber("/recognizer/output", String, get_response, queue_size=10)
    rospy.spin()


#  if __name__ == '__main__':
    #  # 初始化ROS节点以及sound工具
    #  rospy.init_node('aiml_tts', anonymous = True)
    #  #  pub = rospy.Publisher('result', String, queue_size=10)
    #  r = rospy.Rate(1)
    #  soundhandle = SoundClient()
    #  rospy.sleep(1)
    #  soundhandle.stopAll()
    #  print 'Starting TTS'
while not rospy.is_shutdown():
    listener()
    #while not rospy.is_shutdown():


#while not rospy.is_shutdown():
    #listener()
