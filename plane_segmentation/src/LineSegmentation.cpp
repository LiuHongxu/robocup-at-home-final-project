#include <ros/ros.h>
#include <ros/console.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Char.h>
#include <Eigen/Dense>
#include <stdio.h>
#include <cmath>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <tf/transform_listener.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// PCL specific includes
#include <pcl_ros/point_cloud.h> // enable pcl publishing
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/common_headers.h>
#include <image_geometry/pinhole_camera_model.h>

#include <pcl/point_types.h>
#include <geometry_msgs/PoseStamped.h>

using namespace std;
using namespace cv;
//! Lines segmentation class
class LineSegmentation
{

private:
    //! The node handle
    ros::NodeHandle nh_;
    //! Node handle in the private namespace
    ros::NodeHandle priv_nh_;
    //! Subscribers to the PointCloud data
    ros::Subscriber sub_pc;
    //! Publisher for pointclouds
    ros::Publisher pub_line1;
    ros::Publisher pub_line2;
    pcl::PointCloud<pcl::PointXYZ> cloud_line_1_;
    pcl::PointCloud<pcl::PointXYZ> cloud_line_2_;
    float dis1, dis2;
    //------------------ Callbacks -------------------
    void processCloud(const sensor_msgs::PointCloudConstPtr &var);
    // for multiple data topics (const sensor_msgs::TypeConstPtr &var, const sensor_msgs::TypeConstPtr &var, ...)
    float distance(Eigen::Vector4f v1, Eigen::Vector4f v2);
public:
    LineSegmentation(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
    {

        sub_pc = nh_.subscribe("/lidarscan_modified", 10, &LineSegmentation::processCloud, this);
        pub_line1 = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >("/segmentation/line1_points", 10);
        pub_line2 = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >("/segmentation/line2_points", 10);
    }

    ~LineSegmentation() {}
};

//! Callback for processing the Point Cloud data
void LineSegmentation::processCloud(const sensor_msgs::PointCloudConstPtr& input)
{
    sensor_msgs::PointCloud2 cloud2;
    sensor_msgs::convertPointCloudToPointCloud2(*input, cloud2);
    pcl::PointCloud< pcl::PointXYZ > pc; // internal data
    pcl::fromROSMsg(cloud2, pc);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = pc.makeShared(); // cloud to operate

    //std::cout << "PointCloud before filtering has: " << pc.points.size() << " data points." << std::endl;
    //std::cout << "width: " << pc.width << "height: " << pc.height << std::endl;


    // Down sample the pointcloud using VoxelGrid
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered( new pcl::PointCloud<pcl::PointXYZ> );
    sor.setInputCloud(cloud);
    sor.setLeafSize(0.01f, 0.01f, 0.01f);
    sor.filter(*cloud_filtered);   // cloud_filtered is the result after downsampleing
    //std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size()  << " data points." << std::endl;
    
    // Create the segmentation object for the plane model and set all the parameters using pcl::SACSegmentation<pcl::PointXYZ>
    pcl::SACSegmentation<pcl::PointXYZ> seg_1, seg_2;
    pcl::PointIndices::Ptr inliers( new pcl::PointIndices );
    pcl::PointIndices::Ptr inliers_2( new pcl::PointIndices );
    pcl::ModelCoefficients::Ptr coefficients_1( new pcl::ModelCoefficients );
    pcl::ModelCoefficients::Ptr coefficients_2( new pcl::ModelCoefficients );
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line_1( new pcl::PointCloud<pcl::PointXYZ>() );
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line_1_bridge( new pcl::PointCloud<pcl::PointXYZ>() );

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_line_2( new pcl::PointCloud<pcl::PointXYZ>() );

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_clusters( new pcl::PointCloud<pcl::PointXYZ>() );

    // set parameters of the SACS segmentation
    //pcl::PCDWriter writer;
    seg_1.setOptimizeCoefficients(true);
    seg_1.setModelType(pcl::SACMODEL_LINE);// SACMODEL_PLANE);//use the determine plane model for SAC
    seg_1.setMethodType(pcl::SAC_RANSAC);//sample method
    seg_1.setMaxIterations(100);// maximal 100 times iteration
    seg_1.setDistanceThreshold(0.04);


    //int nr_points = (int) cloud_filtered->points.size();

    seg_1.setInputCloud(cloud_filtered);
    seg_1.segment(*inliers, *coefficients_1);
    Eigen::Vector3f axis;
    axis << coefficients_1->values[4], -coefficients_1->values[3], coefficients_1->values[5];

    //Extract the planar inliers from the input cloud
    pcl::ExtractIndices<pcl::PointXYZ> extract, extract_2;
    extract.setInputCloud(cloud_filtered);
    extract.setIndices(inliers);
    extract.setNegative(false);

    extract.filter(*cloud_line_1);

    //remove the lines' inliers, extract the rest
    extract.setNegative(true);
    extract.filter(*cloud_clusters);


    // find the second line based on the coefficient of the first line
    seg_2.setOptimizeCoefficients(true);
    seg_2.setMethodType(pcl::SAC_RANSAC);
    seg_2.setModelType(pcl::SACMODEL_PARALLEL_LINE);
    seg_2.setMaxIterations(100);
    seg_2.setDistanceThreshold(0.1);
    seg_2.setAxis(axis);
    seg_2.setEpsAngle(pcl::deg2rad(2.0));

    seg_2.setInputCloud(cloud_clusters);
    seg_2.segment(*inliers_2, *coefficients_2);
    //ROS_INFO_STREAM("" << *coefficients_2);

    extract_2.setInputCloud(cloud_clusters);
    extract_2.setIndices(inliers_2);
    extract_2.setNegative(false);
    extract_2.filter(*cloud_line_2);

    // keep the overlap points away
    Eigen::Vector4f paramrter_1, paramrter_2;
    paramrter_1 << coefficients_1->values[0], coefficients_1->values[1], coefficients_1->values[3], coefficients_1->values[4];
    paramrter_2 << coefficients_2->values[0], coefficients_2->values[1], coefficients_2->values[3], coefficients_2->values[4];
    //calculate the distance between both lines
    dis1 = LineSegmentation::distance(paramrter_1, paramrter_2);
    dis2 = LineSegmentation::distance(paramrter_2, paramrter_1);
    ROS_INFO_STREAM("distance between both lines are " << dis1 << " or " << dis2);
    if (dis1 > 2.8)
    {
        //----------------for the first line-------------------//
        if (cloud_line_1_.points.size() == 0)
        {
            cloud_line_1_ = *cloud_line_1;
        }
        else
        {
            pcl::PointIndices::Ptr inliers1_(new pcl::PointIndices());
            pcl::ExtractIndices<pcl::PointXYZ> extract1_;

            for(int i = 0; i < cloud_line_1->points.size(); i++)
            {
                for(int j = 0; j < cloud_line_1_.points.size(); j++)
                {
                    float dx1 = cloud_line_1->points[i].x - cloud_line_1_.points[j].x;
                    float dy1 = cloud_line_1->points[i].y - cloud_line_1_.points[j].y;
                    if( sqrt((dx1)*(dx1) + (dy1)*(dy1)) < 0.2 )
                   {
                       inliers1_->indices.push_back(i);
                       break;
                   }
                }
            }
            if(inliers1_->indices.size() <= cloud_line_1->points.size())
            {
                extract1_.setInputCloud(cloud_line_1);
                extract1_.setIndices(inliers1_);
                extract1_.setNegative(true);
                extract1_.filter(*cloud_line_1);
                inliers1_->indices.clear();
            }

            if (cloud_line_1_.points.size() < 20)
            {
                cloud_line_1_ += *cloud_line_1;
            }

        }

        //----------------for the second line-------------------//
        if (cloud_line_2_.points.size() == 0)
        {
            cloud_line_2_ = *cloud_line_2;
        }
        else
        {

            pcl::PointIndices::Ptr inliers2_(new pcl::PointIndices());
            pcl::ExtractIndices<pcl::PointXYZ> extract2_;
            for(int i = 0; i < cloud_line_2->points.size(); i++)
            {
                for(int j = 0; j < cloud_line_2_.points.size(); j++)
                {
                    float dx2 = cloud_line_2->points[i].x - cloud_line_2_.points[j].x;
                    float dy2 = cloud_line_2->points[i].y - cloud_line_2_.points[j].y;
                    if( sqrt((dx2)*(dx2) + (dy2)*(dy2)) < 0.2 )
                    {
                       inliers2_->indices.push_back(i);
                       break;
                    }
                }
            }

            if(inliers2_->indices.size() <= cloud_line_2->points.size())
            {
                extract2_.setInputCloud(cloud_line_2);
                extract2_.setIndices(inliers2_);
                extract2_.setNegative(true);
                extract2_.filter(*cloud_line_2);
                inliers2_->indices.clear();
            }
            if (cloud_line_2_.points.size() < 20)
            {
                cloud_line_2_ += *cloud_line_2;
            }
        }
        ROS_INFO_STREAM("Line 1 has " << cloud_line_1_.points.size() << "data points");
        ROS_INFO_STREAM("Line 2 has " << cloud_line_2_.points.size() << "data points");

        pub_line1.publish(cloud_line_1_);
        pub_line2.publish(cloud_line_2_);
    }

    return;

    // Tips:
	// - you can copy the pointcloud using cl::copyPointCloud()
	// - Set the header frame_id to the pc2 header frame_id
	// - you can use pcl_conversions::toPCL() to convert the stamp from pc2 header to pointcloud header stamp
	// - to publish -> pub_plane_pc_.publish( pointcloud_variable.makeShared() )
    //----


}
float LineSegmentation::distance(Eigen::Vector4f v1, Eigen::Vector4f v2)
{
    float dis_ = ((v2[0]-v1[0])*v1[3]/v1[2] + (v1[1]-v2[1])) * v1[2]/(sqrt(v1[2]*v1[2]+v1[3]*v1[3]));
    if (dis_ <= 0)
    {
        dis_ = -dis_;
    }
    return dis_;
}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "line_segmentation");
    ros::NodeHandle nh;
    LineSegmentation node(nh);
    ros::spin();
    return 0;
}


