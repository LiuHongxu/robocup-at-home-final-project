#include <ros/ros.h>
#include <ros/console.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Char.h>
#include <plane_segmentation/center_points.h>

#include <pcl/filters/passthrough.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// PCL specific includes
#include <pcl_ros/point_cloud.h> // enable pcl publishing
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
#include <image_geometry/pinhole_camera_model.h>

#include <pcl/point_types.h>
#include <geometry_msgs/PoseStamped.h>

#include <pcl/filters/voxel_grid.h>

#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <Eigen/Eigen>
#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <Eigen/Core>

using namespace std;
using namespace cv;


//! Plane segmentation class
//! computes and split the big planes from the rest of the point cloud clusters
class PlaneSegmentation
{

private:
    //! The node handle
    ros::NodeHandle nh_;
    //! Node handle in the private namespace
    ros::NodeHandle priv_nh_;
    bool startWork;
    bool finishWork;
    Eigen::Vector4f center;


    //! Subscribers to the PointCloud data
    // Optional: MESSAGE FILTERS COULD BE A GOOD IDEA FOR GRABBING MULTIPLE TOPICS SYNCRONIZED, NOT NEEDED THOUGH

    ros::Subscriber sub;

    //! Publisher for pointclouds
    ros::Publisher pub;

    ros::Publisher pub_plane_pc_;
    ros::Publisher pub_plane_of_pc_;
    ros::Publisher pub_clusters_pc_, pub_object_pc_ ;
    
    tf::TransformListener listener;
    ros::ServiceServer service;
    //! Parameters

    //! Internal data
    pcl::PointCloud<pcl::PointXYZ> curr_table_pc;
    pcl::PointCloud<pcl::PointXYZ> curr_clusters_pc;


    //------------------ Callbacks -------------------

    //! Callback for service calls


    //! Callback for subscribers
    //! Complete processing of new point cloud
public:
    void processCloud(const sensor_msgs::PointCloud2ConstPtr& input); // for multiple data topics (const sensor_msgs::TypeConstPtr &var, const sensor_msgs::TypeConstPtr &var, ...)

    float distance(float x , float y , float z, std::vector<float> coef) 
    {
        return x*coef[0] + y * coef[1] + z* coef[2] + coef[3];
    };
    bool start_work(plane_segmentation::center_points::Request &req, plane_segmentation::center_points::Response &res);
    static bool cmp (pcl::PointIndices a, pcl::PointIndices b)
    {return a.indices.size() < b.indices.size();};
        
    //! Subscribes to and advertises topics
    PlaneSegmentation(ros::NodeHandle nh) : nh_(nh),startWork(false),finishWork(false), priv_nh_("~") //,
        //sub(nh, "topic", 5) // constructor initialization form for the subscriber if needed
    {

        sub = nh_.subscribe<sensor_msgs::PointCloud2>("/xtion/depth_registered/points", 10, &PlaneSegmentation::processCloud, this);
        pub_plane_pc_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >("/segmentation/without_plane_points", 10);
        pub_clusters_pc_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >("/segmentation/cluster_points", 10);
        pub_object_pc_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >("/segmentation/object_points", 10);
        pub_plane_of_pc_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >("/segmentation/plane_points", 10);
        service=nh_.advertiseService("/DoorHandle", &PlaneSegmentation::start_work, this );
        //initialize params
    }

    ~PlaneSegmentation() {}
};
//! Callback for processing the Point Cloud data
void PlaneSegmentation::processCloud(const sensor_msgs::PointCloud2ConstPtr& input)
{
    if(!startWork) return;

    pcl::PointCloud< pcl::PointXYZ > pc; // internal data


    pcl::fromROSMsg(*input, pc);
  //Convert the data to the internal var (pc) using pcl function: fromROSMsg
  // TODO


    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = pc.makeShared(); // cloud to operate
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f( new pcl::PointCloud<pcl::PointXYZ> ); // cloud to store the filter the data
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_z( new pcl::PointCloud<pcl::PointXYZ> ); // cloud to store the filter the data
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered_x( new pcl::PointCloud<pcl::PointXYZ> ); // cloud to store the filter the data
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud( new pcl::PointCloud<pcl::PointXYZ> ); // cloud to store the filter the data

    
    std::cout << "PointCloud before filtering has: " << pc.points.size() << " data points." << std::endl; //*
    std::cout << "width: " << pc.width << "height: " << pc.height << std::endl;

    tf::StampedTransform time_transform;
    // tf::Transform tf_transform;
    ros::Time now = ros::Time::now();
    // listener.waitForTransform("xtion_rgb_optical_frame", "base_link", now, ros::Duration(3.0));
    // listener.lookupTransform("xtion_rgb_optical_frame", "base_link", now, time_transform );

    listener.waitForTransform("base_link", "xtion_rgb_optical_frame", now, ros::Duration(3.0));
    listener.lookupTransform("base_link","xtion_rgb_optical_frame", now, time_transform );
    tf::Transform tf_transform(time_transform.getBasis(),time_transform.getOrigin());

    Eigen::Affine3d eigen_transform;
    tf::transformTFToEigen(tf_transform, eigen_transform);
    std::cout << "AFFine transformation" << eigen_transform.matrix() <<std::endl;
    pcl::transformPointCloud (*cloud, *transformed_cloud, eigen_transform);
    transformed_cloud->header.frame_id = "base_link";
        

    pcl::VoxelGrid<pcl::PointXYZ> sor;
    sor.setInputCloud(transformed_cloud);
    sor.setLeafSize(0.01, 0.01, 0.01);
    sor.filter (*cloud_f);

    std::cout << "PointCloud after filtering has: " << cloud_f->points.size()  << " data points." << std::endl;


    /*
     * sipangzi,
     */

    pcl::PassThrough<pcl::PointXYZ> pass_z;
    pass_z.setInputCloud (cloud_f);
    pass_z.setFilterFieldName ("z");
    pass_z.setFilterLimits (0.55, 0.7);
    pass_z.filter (*cloud_filtered_z);

   // sensor_msgs::PointCloud2 pointcloud_variable;
   // pcl::PCLPointCloud2 mid;
   // pcl::toPCLPointCloud2(*cloud_filtered_z,mid);
   // pcl_conversions::fromPCL(mid,pointcloud_variable);
   // pub_plane_pc_.publish(pointcloud_variable);
    std::cout << "PointCloud after z pass has: " << cloud_filtered_z->points.size()  << " data points." << std::endl;

    pcl::PassThrough<pcl::PointXYZ> pass_x;
    pass_x.setInputCloud (cloud_filtered_z);
    pass_x.setFilterFieldName ("x");
    pass_x.setFilterLimits (0., 0.8);
    pass_x.setFilterLimitsNegative (true);
    pass_x.filter (*cloud_filtered_x);
    
    // std::cout << "PointCloud after x pass has: " << cloud_filtered_x->points.size()  << " data points." << std::endl;
    /*
     * sipangzi
     */
    // Create the segmentation object for the plane model and set all the parameters using pcl::SACSegmentation<pcl::PointXYZ>
#if 1 
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    pcl::PointIndices::Ptr inliers( new pcl::PointIndices );
    pcl::ModelCoefficients::Ptr coefficients( new pcl::ModelCoefficients );

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_plane( new pcl::PointCloud<pcl::PointXYZ>() );
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_of_plane( new pcl::PointCloud<pcl::PointXYZ>() );
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_object( new pcl::PointCloud<pcl::PointXYZ>() );

    // set parameters of the SACS segmentation
  // TODO

    //----


    // Segment the planes using pcl::SACSegmentation::segment() function and pcl::ExtractIndices::filter() function
  // TODO
        // If you want to extract more than one plane you have to do a while


    //----

    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setDistanceThreshold (0.01);

    seg.setInputCloud (cloud_filtered_x);
    seg.segment (*inliers, *coefficients);
    std::cout << " coefficients size" << coefficients->values.size() <<std::endl;
    float min_x = FLT_MAX;
    float max_x = FLT_MIN;
    for(int i = 0 ; i < inliers->indices.size() ; i++)
    {
       float dis = distance(
               cloud_filtered_x->points[inliers->indices[i]].x,
               cloud_filtered_x->points[inliers->indices[i]].y,
               cloud_filtered_x->points[inliers->indices[i]].z,
               coefficients->values
               );
        min_x = min(dis, min_x);
        max_x = max(dis, max_x);
    }

    // pcl::PassThrough<pcl::PointXYZ> pass_zx;
    // pass_zx.setInputCloud (cloud_filtered_x);
    // pass_zx.setFilterFieldName ("x");
    // pass_zx.setFilterLimits (0., min_x);
    // // pazss_x.setFilterLimitsNegative (true);
    // pass_zx.filter (*cloud_object);
    
    std::cout << "PointCloud after x pass has: " << cloud_object->points.size()  << " data points." << std::endl;
    // pcl_msgs:: ModelCoefficients ros_coefficients;
    // pcl_conversions::fromPCL(*coefficients, ros_coefficients);

    // Publish the model coefficients

    // pub.publish (ros_coefficients);

    // Publish biggest plane
  // TODO
  // Tips:
  // - you can copy the pointcloud using cl::copyPointCloud()
  // - Set the header frame_id to the pc2 header frame_id
  // - you can use pcl_conversions::toPCL() to convert the stamp from pc2 header to pointcloud header stamp
  // - to publish -> pub_plane_pc_.publish( pointcloud_variable.makeShared() )
    //----

   pcl::ExtractIndices<pcl::PointXYZ> extract;

   extract.setInputCloud(cloud_filtered_x);
   extract.setIndices(inliers);
   extract.setNegative(true);
   extract.filter(*cloud_plane);

   extract.setNegative(false);
   extract.filter(*cloud_of_plane);

   sensor_msgs::PointCloud2 pointcloud;
   pcl::PCLPointCloud2 mid1;
   pcl::toPCLPointCloud2(*cloud_of_plane,mid1);
   pcl_conversions::fromPCL(mid1,pointcloud);
   pub_plane_of_pc_.publish(pointcloud);

   pcl::PointIndices::Ptr inliers_handle( new pcl::PointIndices );
   for(int i = 0 ; i < cloud_plane->points.size(); i++)
   {
       float dis = distance(
               cloud_plane->points[i].x,
               cloud_plane->points[i].y,
               cloud_plane->points[i].z,
               coefficients->values
               );
       if(dis < min_x) 
           cloud_object->points.push_back(cloud_plane->points[i]);
           // inliers_handle->indices.push_back(i);
   }

   cloud_object->header.frame_id = "base_link";
   ROS_INFO("OBJECT SIZE %ld", cloud_object->points.size());
   
   // extract.setInputCloud(cloud_plane);
   // extract.setIndices(inliers);
   // extract.setNegative(false);
   // extract.filter(*cloud_object);

   sensor_msgs::PointCloud2 pointcloud_variable;
   pcl::PCLPointCloud2 mid;
   pcl::toPCLPointCloud2(*cloud_plane,mid);
   pcl_conversions::fromPCL(mid,pointcloud_variable);
   pub_plane_pc_.publish(pointcloud_variable);

   // extract.setNegative(true);
   // extract.filter(*cloud_object);
   sensor_msgs::PointCloud2 object;
   pcl::PCLPointCloud2 mid_object;
   pcl::toPCLPointCloud2(*cloud_object,mid_object);
   pcl_conversions::fromPCL(mid_object,object);

   pub_clusters_pc_ .publish(object);

   pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
   tree->setInputCloud(cloud_object);
   std::vector<pcl::PointIndices> cluster_indices;
   pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
   ec.setClusterTolerance(0.02);
   ec.setMinClusterSize(5);
   ec.setMaxClusterSize(25000);
   ec.setSearchMethod(tree);
   ec.setInputCloud(cloud_object);
   ec.extract(cluster_indices);

   ROS_INFO("Cluster SIZE %ld", cluster_indices.size());
   if(cluster_indices.size() == 0) return;
   pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

   cloud_cluster->header.frame_id = "base_link";
   std::vector<pcl::PointIndices> ::iterator max_cluster;
   max_cluster = std::max_element(cluster_indices.begin() , cluster_indices.end() ,cmp);
   ROS_INFO("Max Cluster SIZE %ld", max_cluster->indices.size());
   for(int i = 0 ; i < max_cluster->indices.size() ; i++)
   {
	  cloud_cluster->points.push_back(cloud_object->points[max_cluster->indices[i]]);
   }

   sensor_msgs::PointCloud2 small_cluster;
   pcl::PCLPointCloud2 cluster;
   pcl::toPCLPointCloud2(*cloud_cluster,cluster);
   pcl_conversions::fromPCL(cluster,small_cluster);
   pub_object_pc_.publish(small_cluster);

	pcl::PointXYZ min_pt, max_pt;
	pcl::getMinMax3D(*cloud_cluster,min_pt,max_pt);
	
    pcl::compute3DCentroid(*cloud_cluster,center);
    ROS_INFO_STREAM("center of x:"<<center[0]);
    ROS_INFO_STREAM("center of y:"<<center[1]);
    ROS_INFO_STREAM("center of z:"<<center[2]);
    if(std::isnan(center[0]) || std::isnan(center[1]) || std::isnan(center[2]))
        return;
    finishWork = true;

#endif

    // Publish other clusters
    // TODO similar to the previous publish

    return;

}

bool PlaneSegmentation::start_work(plane_segmentation::center_points::Request &req, plane_segmentation::center_points::Response &res)
{
    if(req.find)
    {
        this->startWork= true;
        ROS_INFO_STREAM("Start Work");
        while(!finishWork)
        {
            ros::Duration(3).sleep();
        }
        res.x = this->center[0];
        res.y = this->center[1];
        res.z = this->center[2];
    }
}
int main(int argc, char** argv)
{
    ros::init(argc, argv, "plane_segmentation");
    ros::AsyncSpinner spinner(5);
    spinner.start();
    ros::NodeHandle nh;
    PlaneSegmentation node(nh);
    ros::waitForShutdown();
    return 0;
}

