cmake_minimum_required(VERSION 2.8.3)
project(plane_segmentation)



## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS  
  roscpp
  rospy
  std_msgs
  cv_bridge
  image_transport
  tf
  tf_conversions
  image_geometry
  message_generation
)
find_package (Eigen3 REQUIRED)
add_definitions(${EIGEN_DEFINITIONS})


add_service_files(
  DIRECTORY srv
  FILES
  center_points.srv
)
#add_message_files(FILES ObjectDetection.msg)
#add_service_files(FILES DetectObject.srv)
generate_messages()



catkin_package(
  # CATKIN_DEPENDS message_runtime
    )
## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}  
)


## Declare a cpp executable
##add_executable(plane_segmentation src/PlaneSegmentation.cpp)
add_executable(doorhandle_segmentation src/DoorHandle_Segmentation.cpp)
##add_dependencies(doorhandle_segmentation plane_segmentation_generate_messages_cpp)
#add_executable(line_segmentation src/LineSegmentation.cpp)

find_package(OpenCV REQUIRED)
find_package(PCL REQUIRED)
include_directories(
  ${PCL_INCLUDE_DIRS}
)
## Specify libraries to link a library or executable target against
#target_link_libraries(plane_segmentation ${catkin_LIBRARIES} ${OpenCV_LIBS} ${PCL_LIBRARIES})
target_link_libraries(doorhandle_segmentation ${catkin_LIBRARIES} ${OpenCV_LIBS} ${PCL_LIBRARIES})
#target_link_libraries(line_segmentation ${catkin_LIBRARIES} ${OpenCV_LIBS} ${PCL_LIBRARIES})


