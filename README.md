# RoboCup at home final project

Robot: Tiago (with mobile base and a 7 DoFs robot arm)  
Task: 
1. Open the drawer of the table, 
2. Pick out an apple out of the drawer, 
3. Hand the apple to people,
4. Shake hands with people 
5. Close the drawer.  

The vedio link:  
<https://drive.google.com/file/d/1N98QmT0jbdV3ER8eUt_yF-OaKldK28z4/view?usp=sharing>
