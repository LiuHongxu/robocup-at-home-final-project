
/************************
Author: Tim
Date: 24.12.2018
ICS Munich Germany
Merry Christmas!
RoboCup Project
Team: Laotie
************************/

// ros library
#include <ros/ros.h>
#include <ros/console.h>

#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/Point.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <std_srvs/Empty.h>

// kdl library
#include <kdl/kdl.hpp>
#include <kdl/chain.hpp>
#include <kdl/tree.hpp>
#include <kdl/segment.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/frames.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/jacobian.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/tf.h>

// C++ library
#include <stdio.h>
#include <iostream>
#include <string>
#include <cmath>
#include <vector>

// Eigen library
#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/SVD>
#include <Eigen/Dense>
// boost headers
#include <boost/shared_ptr.hpp>
#include <cartesian_control/pose_error.h>
#include <cartesian_control/transform.h>
#include <cartesian_control/transform2.h>
// ics_skin library
#include <tum_ics_skin_msgs/SkinCellDataArray.h>
// algorithm library
#include <algorithm>// std::max
using namespace KDL;
using namespace std;
using namespace Eigen;

class Cartesian
{
private:
    // Our Action interface type for moving TIAGo's head, provided as a typedef for convenience
    typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> arm_control_client;
    typedef boost::shared_ptr< arm_control_client>  arm_control_client_Ptr;
    arm_control_client_Ptr ArmClient;
    //ros
    ros::NodeHandle nh_;
    ros::NodeHandle priv_nh_;
    //tf
    tf::TransformListener listener_;
    geometry_msgs::Point p_in, p_out, p_object;

    //Subscriber
    ros::Subscriber joint_state_sub, ft_sub, patch1_sub, patch2_sub, pObject_sub;
    // Publisher
    ros::Publisher joint_position_pub;
    //service
    ros::ServiceServer cartesian_srv, admittance_srv, transform_srv, transform2_srv, transform3_srv, shake_hands_srv;
    //Define parameters!
    double cartesian_vmax, dt;
    int num_sub_path;
    Matrix<double, 6, Dynamic> desired_pose;
    Matrix<double, 7, Dynamic> desired_joint_pose;
    Matrix<double, 7, 1> joint_states;
    Matrix<double, 7, 1> joint_vel;
    Matrix<double, Dynamic, 1> my_x_array;
    Matrix<double, Dynamic, 1> my_y_array;
    Matrix<double, Dynamic, 1> my_z_array;

    Matrix<double, 3, 1> force;
    Matrix<double, 3, 1> torque;

    //double desired_pose [6][num_sub_path];
    //double desired_joint_pose [7][num_sub_path];
    //double my_x_array[num_sub_path];
    //double my_y_array[num_sub_path];
    //double my_z_array[num_sub_path];
    //double *d_pose = &my_array;
    // parameters for the P-Controller
    double k_p;
    // parameters for admittance controller
    double stiffness, damping;
    // vectors for the measured forces and torques from sensor
    // declare jacobian
    Jacobian jacob;
    Tree my_tree;
    bool exit_value, jointState_ready, force_ready, admittance_x_ready, admittance_y_ready;
    Chain chain;
    // solver for the inverse position kinematics that uses Levenberg-Marquardt
    double eps, eps_joints;
    int maxiter;
    unsigned int nj;
    double x_init, y_init, z_init, rx_init, ry_init, rz_init;
    JntArray initialjointpositions;
    ros::Time control_begin;

    double force_init_x, force_init_y, force_init_z;
    bool balance_force;
    double rgb_x, rgb_y, rgb_z, x_object, y_object, z_object;
    // constructor
    struct Result
    {
      double get_x;
      double get_y;
      double get_z;
      double get_rx;
      double get_ry;
      double get_rz;
    };
    struct Patch
    {
        double force;
        double prox;
        double temp;
    };
    Result res;
    Patch patch1, patch2;

    //------Sub_Callback-------//
    void jointStateCb(const sensor_msgs::JointState::ConstPtr& msg);
    void ftSensorCb(const geometry_msgs::WrenchStamped::ConstPtr& msg);
    void patch1_callback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch);
    void patch2_callback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch);
    void pObject_callback(const geometry_msgs::PointConstPtr& point);

    //void patch1_sub(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch);
    //------Service_callback---//
    bool catesian_callback(cartesian_control::pose_error::Request &req, cartesian_control::pose_error::Response &res);
    bool admittance_callback(cartesian_control::pose_error::Request &req, cartesian_control::pose_error::Response &res);
    bool shake_hands_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    bool transform_callback(cartesian_control::transform::Request &req, cartesian_control::transform::Response &res);
    bool transform2_callback(cartesian_control::transform2::Request &req, cartesian_control::transform2::Response &res);
    bool transform3_callback(cartesian_control::transform2::Request &req, cartesian_control::transform2::Response &res);
    //---callback--//
    void initialize();
    double create_trajectory(double t, double vmax, double d);
    double generate_velocity(double t, double vmax, double d);
    //void generate_trajectory(double vmax, double pose_diff, int num_interpolation);
    Result get_realtime_pose(JntArray joint_pos, Chain chain);
    VectorXd generate_trajectory (double vmax, double pose_diff, int num_interpolation);
    void get_jacobian(JntArray joint_pos, Chain chain);
    void pseudoInverse(const Eigen::MatrixXd& M_, Eigen::MatrixXd& M_pinv_, bool damped);
    double admittance_controller(double error, double f, double k_p_, double v_d);
    void createArmClient(arm_control_client_Ptr& actionClient);
    void waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, JntArray joint_d);
    void waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, MatrixXd desired_joint_pose, int num_sub_path);

public:
    Cartesian(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
    {
        //initialize parameters!
        admittance_x_ready = false;
        admittance_y_ready = false;
        Cartesian::initialize();
        eps = 1E-5;
        maxiter = 500;
        eps_joints = 1E-15;
        patch2.force = 0;
        patch2.prox = 0;
        patch2.temp = 0;
        force_init_x = 0;
        force_init_y = 0;
        force_init_z = 0;
        balance_force = true;
        // obtain kdl_tree of robot arm from urdf file
        kdl_parser::treeFromFile("/home/tiago/ros/workspaces/tiago_ws/src/tiago/tiago_robot/tiago_description/urdf/arm/arm.urdf", my_tree);
        exit_value = my_tree.getChain("torso_lift_link", "arm_tool_link", chain);

        // initialize the joint positions
        nj = chain.getNrOfJoints();
        initialjointpositions = JntArray(nj);
        //arm service client
        Cartesian::createArmClient(ArmClient);
        // Precondition: Valid clock
        if (!ros::Time::waitForValid(ros::WallDuration(10.0))) // NOTE: Important when using simulated clock
        {
          ROS_FATAL("Timed-out waiting for valid time.");
        }
        // subscribe /joint_states topic to obtain the joint states in real time
        joint_state_sub = nh_.subscribe("/joint_states", 1, &Cartesian::jointStateCb, this);
        // subscribe /wrist_ft topic to obtain the force and torque from F/T sensor
        ft_sub = nh_.subscribe("/wrist_ft", 1, &Cartesian::ftSensorCb, this);
        patch1_sub = nh_.subscribe("/tiago/patch1", 1, &Cartesian::patch1_callback, this);
        patch2_sub = nh_.subscribe("/tiago/patch2", 1, &Cartesian::patch2_callback, this);
        pObject_sub = nh_.subscribe("/segmentation/point3D", 1, &Cartesian::pObject_callback, this);
        // publish joint positions in realtime
        joint_position_pub = nh_.advertise<trajectory_msgs::JointTrajectory>("/arm_controller/safe_command", 10);
        //service
        cartesian_srv = nh_.advertiseService("/cartesian_service", &Cartesian::catesian_callback, this);
        admittance_srv = nh_.advertiseService("/admittance_service", &Cartesian::admittance_callback, this);
        shake_hands_srv = nh_.advertiseService("/shake_hands_service", &Cartesian::shake_hands_callback, this);
        transform_srv = nh_.advertiseService("/transform_service", &Cartesian::transform_callback, this);
        transform2_srv = nh_.advertiseService("/transform2_service", &Cartesian::transform2_callback, this);
        transform3_srv = nh_.advertiseService("/transform3_service", &Cartesian::transform3_callback, this);

        //waiting for both subscriber get ready
        while( !jointState_ready)
        {
            ROS_INFO("Joint_sub or Force_sub is not ready");
        }
        ROS_INFO("Services are ready to be called");

    }
    ~Cartesian(){}
};

// initialize all parameters
void Cartesian::initialize()
{
    joint_states.setZero();
    joint_vel.setZero();
    num_sub_path = 5;
    desired_pose.setZero();
    desired_joint_pose.setZero();
    my_x_array.setZero();
    my_y_array.setZero();
    my_z_array.setZero();
    cartesian_vmax = 0.05;
    dt = 0.03;
    // parameters for the P-Controller
    //k_p = 10.0;
    // parameters for admittance controller
    //stiffness = 600.0;
    //damping = 10.0;
    // vectors for the measured forces and torques from sensor
    force.setZero();
    torque.setZero();
    //subscriber flags
    jointState_ready = false;
    force_ready = false;
    // solver for the inverse position kinematics that uses Levenberg-Marquardt
}


void Cartesian::pObject_callback(const geometry_msgs::PointConstPtr& point)
{
  rgb_x = point->x;
  rgb_y = point->y;
  rgb_z = point->z;
}

bool Cartesian::catesian_callback(cartesian_control::pose_error::Request &req, cartesian_control::pose_error::Response &res)
{
    for (int j = 0; j < nj; j++)
    {
      initialjointpositions(j) = joint_states[j];
    }
    //ROS_INFO("JOINTS: %f, %f, %f, %f, %f, %f, %f", initialjointpositions(0), initialjointpositions(1), initialjointpositions(2),
    //          initialjointpositions(3), initialjointpositions(4), initialjointpositions(5), initialjointpositions(6));
    Result pose_cur, pose_d, pose_error;
    pose_d, pose_cur = Cartesian::get_realtime_pose(initialjointpositions, chain);
    ROS_INFO_STREAM(" " << pose_d.get_x);
    //ROS_INFO_STREAM("chain " << );
    ROS_INFO_STREAM(__LINE__);
    pose_d.get_x += req.x_error;
    pose_d.get_y += req.y_error;
    pose_d.get_z += req.z_error;
    pose_d.get_rx += req.rx_error;
    pose_d.get_ry += req.ry_error;
    pose_d.get_rz += req.rz_error;
    ROS_INFO_STREAM(" " << pose_d.get_x);

    // fill in the desired pose array (generate desired trajectory)
    // obtain the largest difference among x/y/z axis
    double max_p_diff = (fabs(req.x_error) > fabs(req.y_error)) ? (req.x_error) : (req.y_error);
    int max_index = (fabs(req.x_error) > fabs(req.y_error)) ? 0 : 1;
    max_p_diff = ( fabs(max_p_diff) > fabs(req.z_error)) ? max_p_diff : (req.z_error);
    max_index = (fabs(max_p_diff) > fabs(req.z_error)) ? max_index : 2;

    double t_duration = 4 * fabs(max_p_diff) / 3 / cartesian_vmax;

    // generate the desired trajectory for the direction with largest error
    VectorXd x_traj = my_x_array;
    VectorXd y_traj = my_y_array;
    VectorXd z_traj = my_z_array;
    ROS_INFO_STREAM("max_index is " << max_index);

    switch (max_index)
    {
      case 0: // x_error largest
        {
          x_traj = Cartesian::generate_trajectory(cartesian_vmax, max_p_diff, num_sub_path);
          y_traj = Cartesian::generate_trajectory(4 * fabs(req.y_error) / 3 / t_duration, req.y_error + 0.0001, num_sub_path);
          z_traj = Cartesian::generate_trajectory(4 * fabs(req.z_error) / 3 / t_duration, req.z_error + 0.0001, num_sub_path);
        }
        break;
      case 1: // y_error largest
        {
          y_traj = Cartesian::generate_trajectory(cartesian_vmax, max_p_diff, num_sub_path);
          x_traj = Cartesian::generate_trajectory(4 * fabs(req.x_error) / 3 / t_duration, req.x_error + 0.0001, num_sub_path);
          z_traj = Cartesian::generate_trajectory(4 * fabs(req.z_error) / 3 / t_duration, req.z_error + 0.0001, num_sub_path);
        }
        break;
      case 2: // z_error largest
        {
          z_traj = Cartesian::generate_trajectory(cartesian_vmax, max_p_diff, num_sub_path);
          y_traj = Cartesian::generate_trajectory(4 * fabs(req.y_error) / 3 / t_duration, req.y_error + 0.0001, num_sub_path);
          x_traj = Cartesian::generate_trajectory(4 * fabs(req.x_error) / 3 / t_duration, req.x_error + 0.0001, num_sub_path);
        }
        break;
    }

    //ROS_INFO_STREAM("x_traj: " << x_traj);
    //ROS_INFO_STREAM("y_traj: " << y_traj);
    //ROS_INFO_STREAM("z_traj: " << z_traj);

    //ROS_INFO_STREAM("Desired Pose Trajectory: " << desired_pose[0][0] << desired_pose[0][1] << desired_pose[0][2]  << desired_pose[0][3] << desired_pose[0][4]);
    // generate corresponding joint trajectories
    for (int n_interpolation = 0; n_interpolation < num_sub_path; n_interpolation++)
    {
      //ROS_INFO_STREAM("x_traj: " << x_traj);
      //ROS_INFO_STREAM("y_traj: " << y_traj);
      //ROS_INFO_STREAM("z_traj: " << z_traj);

      //ROS_INFO("interpolation: %d", n_interpolation);
      Vector vector = Vector(x_traj[n_interpolation] + pose_cur.get_x, y_traj[n_interpolation] + pose_cur.get_y, z_traj[n_interpolation] + pose_cur.get_z);
      //ROS_INFO_STREAM("" << vector);
      double roll = pose_cur.get_rx;
      double pitch = pose_cur.get_ry;//ry_d;
      double yaw = pose_cur.get_rz;//rz_d;
      // compute rotation matrix
      double rot0 = cos(yaw) * cos(pitch);
      double rot1 = cos(yaw) * sin(pitch) * sin(roll) - sin(yaw) * cos(roll);
      double rot2 = cos(yaw) * sin(pitch) * cos(roll) + sin(yaw) * sin(roll);
      double rot3 = sin(yaw) * cos(pitch);
      double rot4 = sin(yaw) * sin(pitch) * sin(roll) + cos(yaw) * cos(roll);
      double rot5 = sin(yaw) * sin(pitch) * cos(roll) - cos(yaw) * sin(roll);
      double rot6 = -sin(pitch);
      double rot7 = cos(pitch) * sin(roll);
      double rot8 = cos(pitch) * cos(roll);

      Rotation rot = Rotation(rot0, rot1, rot2, rot3, rot4, rot5, rot6, rot7, rot8);

      // desired catesian pose
      Frame cartesian_pose = Frame(rot, vector);
      // updated joint positions
      JntArray jointpositions = JntArray(nj);
      ChainIkSolverPos_LMA iksolver = ChainIkSolverPos_LMA(chain, eps, maxiter, eps_joints);
      bool kinematics_status;
      // calculate desired joint positions from desired catesian pose
      kinematics_status = iksolver.CartToJnt(initialjointpositions, cartesian_pose, jointpositions);

      if (kinematics_status >= 0)
      {
        //ROS_INFO("Joint positions should be: %f, %f, %f, %f, %f, %f, %f", jointpositions(0), jointpositions(1),
        //          jointpositions(2), jointpositions(3), jointpositions(4), jointpositions(5), jointpositions(6));
        //ROS_INFO("Successfully obtain the desired joint positions!");
      }
      else
      {
        ROS_WARN("Error: Could not find corresponding inverse kinematics!");
        return -1;
      }

      //desired_joint_pose.cols(n_interpolation) << jointpositions(0), jointpositions(1), jointpositions(2), jointpositions(3), jointpositions(4), jointpositions(5), jointpositions(6);
      desired_joint_pose.resize(7, num_sub_path);
      for (int j = 0; j < 7; j++)
      {
          desired_joint_pose(j, n_interpolation) = jointpositions(j);
      }
      //ROS_INFO_STREAM("" << desired_joint_pose);
    }

    control_msgs::FollowJointTrajectoryGoal arm_goal;
    Cartesian::waypoints_arm_goal(arm_goal, desired_joint_pose, num_sub_path);
    // Sends the command to start the given trajectory 1s from now
    arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
    ROS_INFO_STREAM("" << arm_goal);
    ArmClient->sendGoal(arm_goal);

    // Wait for trajectory execution
    while(!(ArmClient->getState().isDone()) && ros::ok())
    {
      ros::Duration(0.5).sleep(); // sleep for four seconds
      ROS_INFO("SUCCESS!");
    }
    return res.done = true;
}


bool Cartesian::admittance_callback(cartesian_control::pose_error::Request &req, cartesian_control::pose_error::Response &res)
{
    ros::Rate loop_r = 50;

    balance_force = true;
    
    control_begin = ros::Time::now();

    for (int j = 0; j < nj; j++)
    {
      initialjointpositions(j) = joint_states[j];
    }
    //ROS_INFO("JOINTS: %f, %f, %f, %f, %f, %f, %f", initialjointpositions(0), initialjointpositions(1), initialjointpositions(2),
    //          initialjointpositions(3), initialjointpositions(4), initialjointpositions(5), initialjointpositions(6));
    Result pose_cur, pose_d, pose_error;
    pose_d = Cartesian::get_realtime_pose(initialjointpositions, chain);
    pose_d.get_x += req.x_error;
    pose_d.get_y += req.y_error;
    pose_d.get_z += req.z_error;
    pose_d.get_rx += req.rx_error;
    pose_d.get_ry += req.ry_error;
    pose_d.get_rz += req.rz_error;
    Cartesian::get_jacobian(initialjointpositions, chain);
    Eigen::MatrixXd jacob_pinv;
    //obtain the pseudo inverse matrix of transpose of the jacobian matrix
    Cartesian::pseudoInverse(jacob.data, jacob_pinv, true);
    ROS_INFO_STREAM(jacob_pinv);    
    pose_error.get_x = req.x_error;
    pose_error.get_y = req.y_error;
    pose_error.get_z = req.z_error;
    pose_error.get_rx = req.rx_error;
    pose_error.get_ry = req.ry_error;
    pose_error.get_rz = req.rz_error;

    int back_times = 0;

    while(fabs(pose_error.get_y) > 0.01 || fabs(pose_error.get_x) > 0.01 || fabs(pose_error.get_z) > 0.01)
    {

      double force_z = force[2];
      double force_y = force[1];
      double force_x = force[0];
      

      double v_desired_x = Cartesian::generate_velocity((ros::Time::now() - control_begin).toSec(), 0.03, req.x_error); 
      double v_desired_z = Cartesian::generate_velocity((ros::Time::now() - control_begin).toSec(), 0.03, req.z_error);
      //double v_desired_y = Cartesian::generate_velocity((ros::Time::now() - control_begin).toSec(), 0.03, req.y_error);

      double k_stiffness_x;

      if (force_z < -2)
        k_stiffness_x = 5 * req.kp_d;
      else
        k_stiffness_x = req.kp_d;

      // if (force_y < -2)
      //   k_stiffness_y = 5 * req.kp_d;
      // else
      //   k_stiffness_y = req.kp_d;
      

      //double y_dot = Cartesian::admittance_controller(pose_error.get_y, force_x, k_stiffness_y, v_desired_y);

      //ROS_INFO_STREAM("Pose error: " << pose_error.get_x);

      //ROS_INFO_STREAM("Desired velocity: " << v_desired << ", x_dot: " << x_dot);
      //ROS_INFO_STREAM("Force: " << force_z);

      double y_dot = 5 * pose_error.get_y;
      double z_dot = 0;
      double x_dot = 0;

      if (!req.pregrasp)
      {
          x_dot = Cartesian::admittance_controller(pose_error.get_x, force_z, k_stiffness_x, v_desired_x);
          z_dot = pose_error.get_z;
      }
      else
      {
        x_dot = 5 * pose_error.get_x;
        z_dot = Cartesian::admittance_controller(pose_error.get_z, -force_z, req.kp_d, v_desired_z);
      }
      
      
	    
      //ROS_INFO("x_dot: %f, y_dot: %f", x_dot, y_dot);
      Eigen::VectorXd p_dot(6);
      p_dot << x_dot, y_dot, z_dot, 0.0, 0.0, 0.0;
      Eigen::VectorXd q_dot(7);
      q_dot = jacob_pinv * p_dot;
      //ROS_INFO_STREAM(q_dot);
      // update the desired joint positions
      JntArray jointpositions_updated = JntArray(nj);
      for (int i = 0; i < nj; i++)
      {
        if (!req.pregrasp)
        {
          jointpositions_updated(i) = joint_states[i] + q_dot(i) * 0.01;
        }
        else
        {
          jointpositions_updated(i) = joint_states[i] + q_dot(i) * 0.015;
        }
        
        //initialjointpositions(i) =jointpositions_updated(i);
      }
      //ROS_INFO("Updated joint positions should be: %f, %f, %f, %f, %f, %f, %f", jointpositions_updated(0), jointpositions_updated(1), jointpositions_updated(2),
        //                    jointpositions_updated(3), jointpositions_updated(4), jointpositions_updated(5), jointpositions_updated(6));

      // using topic to garantee the realtime-control
      trajectory_msgs::JointTrajectory arm_goal;
      arm_goal.header.stamp = ros::Time::now();
      arm_goal.joint_names.push_back("arm_1_joint");
      arm_goal.joint_names.push_back("arm_2_joint");
      arm_goal.joint_names.push_back("arm_3_joint");
      arm_goal.joint_names.push_back("arm_4_joint");
      arm_goal.joint_names.push_back("arm_5_joint");
      arm_goal.joint_names.push_back("arm_6_joint");
      arm_goal.joint_names.push_back("arm_7_joint");

      trajectory_msgs::JointTrajectoryPoint goal_point;

      goal_point.positions.resize(7);
      goal_point.positions[0] = jointpositions_updated(0);
      goal_point.positions[1] = jointpositions_updated(1);
      goal_point.positions[2] = jointpositions_updated(2);
      goal_point.positions[3] = jointpositions_updated(3);
      goal_point.positions[4] = jointpositions_updated(4);
      goal_point.positions[5] = jointpositions_updated(5);
      goal_point.positions[6] = jointpositions_updated(6);

      // Velocities
      goal_point.velocities.resize(7);
      for (int j = 0; j < 7; ++j)
      {
        goal_point.velocities[j] = 0;
      }

      goal_point.time_from_start = ros::Duration(1.0);

      arm_goal.points.resize(1);
      arm_goal.points[0] = goal_point;

      joint_position_pub.publish(arm_goal);

/*
      // Generates the goal for the TIAGo's arm
      control_msgs::FollowJointTrajectoryGoal arm_goal;
      Cartesian::waypoints_arm_goal(arm_goal, jointpositions_updated);
      //Cartesian::waypoints_arm_goal(arm_goal, desired_joint_pose, num_sub_path);
      //Cartesian::waypoints_arm_goal(arm_goal);

      // Sends the command to start the given trajectory 1s from now
      arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
      ROS_INFO_STREAM("" << arm_goal);
      ArmClient->sendGoal(arm_goal);
      // Wait for trajectory execution
      while(!(ArmClient->getState().isDone()) && ros::ok())
      {
        ros::Duration(0.02).sleep();
        ROS_INFO("SUCCESS!");
      }
*/
      for (int j = 0; j < nj; j++)
      {
        initialjointpositions(j) = joint_states[j];
      }
      //calculate the current pose error
      pose_cur = Cartesian::get_realtime_pose(initialjointpositions, chain);
      pose_error.get_x = pose_d.get_x - pose_cur.get_x;
      pose_error.get_y = pose_d.get_y - pose_cur.get_y;
      pose_error.get_z = pose_d.get_z - pose_cur.get_z;

      if (!req.pregrasp)
      {
        if (fabs(pose_error.get_y) < 0.03)  // arrive the desired position for grasping the handle
        {
          admittance_y_ready = true;
          //ROS_INFO_STREAM("Proximity sensored to the desk: " << patch2.prox << ", Force sensored: " << patch2.force);
          if (patch2.prox > 0.25)  // check whether the distance between gripper and desk is suitable
          {
            // end of the service
            return res.done = true;
          }
        }
      }
      else
      {
        if (fabs(pose_error.get_x) < 0.02 && fabs(pose_error.get_y) < 0.02 && ( fabs(pose_error.get_z) < 0.01 || patch2.prox > 0.5) )// && patch2.prox > 0.5)
        {
          return res.done = true;
        }
      }
      loop_r.sleep();
    }

}


bool Cartesian::shake_hands_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ros::Rate loop_r = 50;

    balance_force = true;
    
    control_begin = ros::Time::now();

    for (int j = 0; j < nj; j++)
    {
      initialjointpositions(j) = joint_states[j];
    }
    //ROS_INFO("JOINTS: %f, %f, %f, %f, %f, %f, %f", initialjointpositions(0), initialjointpositions(1), initialjointpositions(2),
    //          initialjointpositions(3), initialjointpositions(4), initialjointpositions(5), initialjointpositions(6));
    Result pose_cur, pose_d, pose_error;
    pose_d = Cartesian::get_realtime_pose(initialjointpositions, chain);
    pose_d.get_x += 0.01;
    pose_d.get_y += 0.01;
    pose_d.get_z -= 0.05;
    pose_d.get_rx = pose_cur.get_rx;
    pose_d.get_ry = pose_cur.get_ry;
    pose_d.get_rz = pose_cur.get_rz;
    Cartesian::get_jacobian(initialjointpositions, chain);
    Eigen::MatrixXd jacob_pinv;
    //obtain the pseudo inverse matrix of transpose of the jacobian matrix
    Cartesian::pseudoInverse(jacob.data, jacob_pinv, true);
    //ROS_INFO_STREAM(jacob_pinv);    
    pose_error.get_x = 0.01;
    pose_error.get_y = 0.01;
    pose_error.get_z = -0.05;
    pose_error.get_rx = 0;
    pose_error.get_ry = 0;
    pose_error.get_rz = 0;


    while((ros::Time::now() - control_begin).toSec() < 30)
    {
      double force_z = force[2];
      double force_y = force[1];
      double force_x = force[0];

      //ROS_INFO_STREAM("force_x: " << force_x << ", force_y: " << force_y << ", force_z: " << force_z );
      
      double x_dot = Cartesian::admittance_controller(pose_error.get_x, force_z, 1.0, 0.005);
      double y_dot = Cartesian::admittance_controller(pose_error.get_y, force_x, 1.0, 0.005);
      double z_dot = Cartesian::admittance_controller(pose_error.get_z, force_y, 1.0, 0.005);
      
      Eigen::VectorXd p_dot(6);
      p_dot << x_dot, y_dot, z_dot, 0.0, 0.0, 0.0;
      Eigen::VectorXd q_dot(7);
      q_dot = jacob_pinv * p_dot;
      //ROS_INFO_STREAM(q_dot);
      // update the desired joint positions
      JntArray jointpositions_updated = JntArray(nj);
      for (int i = 0; i < nj; i++)
      {
          jointpositions_updated(i) = joint_states[i] + q_dot(i) * 0.01;
        
      }
      //ROS_INFO("Updated joint positions should be: %f, %f, %f, %f, %f, %f, %f", jointpositions_updated(0), jointpositions_updated(1), jointpositions_updated(2),
      //                      jointpositions_updated(3), jointpositions_updated(4), jointpositions_updated(5), jointpositions_updated(6));

      // using topic to garantee the realtime-control
      trajectory_msgs::JointTrajectory arm_goal;
      arm_goal.header.stamp = ros::Time::now();
      arm_goal.joint_names.push_back("arm_1_joint");
      arm_goal.joint_names.push_back("arm_2_joint");
      arm_goal.joint_names.push_back("arm_3_joint");
      arm_goal.joint_names.push_back("arm_4_joint");
      arm_goal.joint_names.push_back("arm_5_joint");
      arm_goal.joint_names.push_back("arm_6_joint");
      arm_goal.joint_names.push_back("arm_7_joint");

      trajectory_msgs::JointTrajectoryPoint goal_point;

      goal_point.positions.resize(7);
      goal_point.positions[0] = jointpositions_updated(0);
      goal_point.positions[1] = jointpositions_updated(1);
      goal_point.positions[2] = jointpositions_updated(2);
      goal_point.positions[3] = jointpositions_updated(3);
      goal_point.positions[4] = jointpositions_updated(4);
      goal_point.positions[5] = jointpositions_updated(5);
      goal_point.positions[6] = jointpositions_updated(6);

      // Velocities
      goal_point.velocities.resize(7);
      for (int j = 0; j < 7; ++j)
      {
        goal_point.velocities[j] = 0;
      }

      goal_point.time_from_start = ros::Duration(1.0);

      arm_goal.points.resize(1);
      arm_goal.points[0] = goal_point;

      joint_position_pub.publish(arm_goal);

      for (int j = 0; j < nj; j++)
      {
        initialjointpositions(j) = joint_states[j];
      }
      //calculate the current pose error
      pose_cur = Cartesian::get_realtime_pose(initialjointpositions, chain);
      pose_error.get_x = pose_d.get_x - pose_cur.get_x;
      pose_error.get_y = pose_d.get_y - pose_cur.get_y;
      pose_error.get_z = pose_d.get_z - pose_cur.get_z;

      loop_r.sleep();
    }

    return true;

}



bool Cartesian::transform_callback(cartesian_control::transform::Request &req, cartesian_control::transform::Response &res)
{
    geometry_msgs::PointStamped p_in_s, p_out_s;
    p_in_s.header.frame_id = "base_link";
    p_in_s.point.x = req.x_in;
    p_in_s.point.y = req.y_in;
    p_in_s.point.z = req.z_in;

    listener_.transformPoint("gripper_grasping_frame", p_in_s, p_out_s);

    res.x_out = p_out_s.point.x;
    res.y_out = p_out_s.point.y;
    res.z_out = p_out_s.point.z;
    return true;
}


bool Cartesian::transform2_callback(cartesian_control::transform2::Request &req, cartesian_control::transform2::Response &res)
{
    geometry_msgs::PointStamped p_in_s, p_out_s;
    p_in_s.header.frame_id = "xtion_rgb_optical_frame";
    p_in_s.point.x = x_object;
    p_in_s.point.y = y_object;
    p_in_s.point.z = z_object;

    listener_.transformPoint("gripper_grasping_frame", p_in_s, p_out_s);

    res.x_out = p_out_s.point.x;
    res.y_out = p_out_s.point.y;
    res.z_out = p_out_s.point.z;
    return true;
}

bool Cartesian::transform3_callback(cartesian_control::transform2::Request &req, cartesian_control::transform2::Response &res)
{
    geometry_msgs::PointStamped p_in_s, p_out_s;
    p_in_s.header.frame_id = "xtion_rgb_optical_frame";
    p_in_s.point.x = rgb_x;
    p_in_s.point.y = rgb_y;
    p_in_s.point.z = rgb_z;
    x_object = rgb_x;
    y_object = rgb_y;
    z_object = rgb_z;

    listener_.transformPoint("base_link", p_in_s, p_out_s);

    res.x_out = p_out_s.point.x;
    res.y_out = p_out_s.point.y;
    res.z_out = p_out_s.point.z;
    return true;
}




// Create a ROS action client to move TIAGo's arm
void Cartesian::createArmClient(arm_control_client_Ptr& actionClient)
{
  ROS_INFO("Creating action client to arm controller ...");

  actionClient.reset( new arm_control_client("/arm_controller/follow_joint_trajectory") );

  int iterations = 0, max_iterations = 3;
  // Wait for arm controller action server to come up
  while( !actionClient->waitForServer(ros::Duration(2.0)) && ros::ok() && iterations < max_iterations )
  {
    ROS_DEBUG("Waiting for the arm_controller_action server to come up");
    ++iterations;
  }

  if ( iterations == max_iterations )
    throw std::runtime_error("Error in createArmClient: arm controller action server not available");
}


void Cartesian::jointStateCb(const sensor_msgs::JointState::ConstPtr& msg)
{
    vector<string> joint_name = msg->name;
    vector<double> joint_position = msg->position;
    vector<double> joint_velocity = msg->velocity;
    unsigned int num_joint = joint_name.size();
    for (int i = 0; i < num_joint; i ++)
    {
      if (joint_name[i] == "arm_1_joint"){
        joint_states[0] = joint_position[i];
        joint_vel[0] = joint_velocity[i];
      }
      if (joint_name[i] == "arm_2_joint"){
        joint_states[1] = joint_position[i];
        joint_vel[1] = joint_velocity[i];
      }
      if (joint_name[i] == "arm_3_joint"){
        joint_states[2] = joint_position[i];
        joint_vel[2] = joint_velocity[i];
      }
      if (joint_name[i] == "arm_4_joint"){
        joint_states[3] = joint_position[i];
        joint_vel[3] = joint_velocity[i];
      }
      if (joint_name[i] == "arm_5_joint"){
        joint_states[4] = joint_position[i];
        joint_vel[4] = joint_velocity[i];
      }
      if (joint_name[i] == "arm_6_joint"){
        joint_states[5] = joint_position[i];
        joint_vel[5] = joint_velocity[i];
      }
      if (joint_name[i] == "arm_7_joint"){
        joint_states[6] = joint_position[i];
        joint_vel[6] = joint_velocity[i];
      }
    }
    jointState_ready = true;
}

void Cartesian::ftSensorCb(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{
  // add offset of the forces
  if (balance_force)
  {
    force_init_x = msg->wrench.force.x;
    force_init_y = msg->wrench.force.y;
    force_init_z = msg->wrench.force.z;
    balance_force = false;
  }

  force[0] = msg->wrench.force.x - force_init_x + 0.1;
  force[1] = msg->wrench.force.y - force_init_y + 0.1;
  force[2] = msg->wrench.force.z - force_init_z + 0.1;

  torque[0] = msg->wrench.torque.x;
  torque[1] = msg->wrench.torque.y;
  torque[2] = msg->wrench.torque.z;
  force_ready = true;
}


VectorXd Cartesian::generate_trajectory(double vmax, double pose_diff, int num_interpolation)
{
  // generate a trapezoidal motion path
  double t_from_start = 0;
  double t_end = 4 * fabs(pose_diff) / 3 / vmax;
  double delta_t = (t_end - t_from_start) / num_interpolation;
  VectorXd d_pose(num_sub_path);
  for (int index = 0; index < num_interpolation; index++)
  {
    t_from_start = delta_t * (index + 1);

    //*(d_pose + index) = Cartesian::create_trajectory(t_from_start, vmax, pose_diff);
    d_pose(index) = Cartesian::create_trajectory(t_from_start, vmax, pose_diff);

  }
  return d_pose;
}

double Cartesian::create_trajectory(double t, double vmax, double d)
{
  double trajectory = 0;
  double fabs_d =fabs(d);
  // first phase: acceleration
  if (t < fabs(d) / 3 / vmax)
    trajectory = 3 * pow(vmax * t, 2) / 2 / d;  
  // second phase: constant speed
  else if (t < fabs(d) / vmax)
    trajectory = d / fabs_d * (t * vmax - fabs_d / 6);
  // third phase: deceleration
  else
    trajectory = d / fabs_d *(fabs_d - 3 / 2 / fabs_d * pow(vmax * (4 * fabs_d / 3 / vmax - t), 2));
  return trajectory;
}

double Cartesian::generate_velocity(double t, double vmax, double d)
{
  double fabs_d = fabs(d);
  double v_now = 0;
  // first phase: acceleration
  if (t < fabs(d) / 3 / vmax)
    v_now = 3 * pow(vmax, 2) / d * t;
  // second phase: constant speed
  else if (t < fabs(d) / vmax)
    v_now = d / fabs_d * vmax;
  // third phase: deceleration
  else if (t < 4 * fabs(d) / 3 / vmax)
    v_now = (4 * fabs_d / 3 / vmax - t) * 3 * pow(vmax, 2) / d * (4 * fabs(d) / 3 / vmax - t);
  else
    v_now = 0;
  
  return v_now;
}

// obtain jacobian from chain and joint positions
void Cartesian::get_jacobian(JntArray joint_pos, Chain chain)
{
  // solver for computing the jacobian
  ChainJntToJacSolver jnt_to_jac_solver = ChainJntToJacSolver(chain);
  // compute the jacobian expressed in the base frame of the chain
  jacob.resize(7); // neccessary to allocate memory for new size
  bool jnt_to_jac_status;
  jnt_to_jac_status = jnt_to_jac_solver.JntToJac(joint_pos, jacob);
}

// function for computing pseudo inverse matrix
// using SVD decomposition
void Cartesian::pseudoInverse(const Eigen::MatrixXd& M_, Eigen::MatrixXd& M_pinv_, bool damped)
{
  double lambda_ = damped ? 0.2 : 0.0;

  Eigen::JacobiSVD<Eigen::MatrixXd> svd(M_, Eigen::ComputeFullU | Eigen::ComputeFullV);
  Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType sing_vals_ = svd.singularValues();
  Eigen::MatrixXd S_ = M_;  // copying the dimensions of M_, its content is not needed.
  S_.setZero();
  for (int i = 0; i < sing_vals_.size(); i++)
    S_(i, i) = (sing_vals_(i)) / (sing_vals_(i) * sing_vals_(i) + lambda_ * lambda_);

  M_pinv_ = Eigen::MatrixXd(svd.matrixV() * S_.transpose() * svd.matrixU().transpose());
}

// apply simple P-Controller and admittance controller on the cartesian pose
// error = desired - actual (k_p > 0)
double Cartesian::admittance_controller(double error, double f, double k_p_, double v_d)
{
  double dx = v_d + f + k_p_ * error;
  return dx;
}

Cartesian::Result Cartesian::get_realtime_pose(JntArray joint_pos, Chain chain)
{
  // Implementation of a recursive forward position kinematics algorithm 
  // to calculate the position transformation from joint space to Cartesian space of a general kinematic chain
  ChainFkSolverPos_recursive fksolver = ChainFkSolverPos_recursive(chain);
  // compute the pose of end effector with forward kinematics
  Frame cartesian_pose;
  bool kinematics_status_forward;
  kinematics_status_forward = fksolver.JntToCart(joint_pos, cartesian_pose);  

  double x = cartesian_pose(0, 3);
  double y = cartesian_pose(1, 3);
  double z = cartesian_pose(2, 3);
  double rx = atan2(cartesian_pose(2, 1), cartesian_pose(2, 2));
  double ry = atan2(-cartesian_pose(2, 0), sqrt(pow(cartesian_pose(2, 1), 2) + pow(cartesian_pose(2, 2), 2)));
  double rz = atan2(cartesian_pose(1, 0), cartesian_pose(0, 0));
  Result ret;
  ret.get_x = x;
  ret.get_y = y;
  ret.get_z = z;
  ret.get_rx = rx;
  ret.get_ry = ry;
  ret.get_rz = rz;
  
  return ret;
}

// Generates a simple trajectory with two waypoints to move TIAGo's arm
void Cartesian::waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, JntArray joint_d)
{
  // The joint names, which apply to all waypoints
  goal.trajectory.joint_names.push_back("arm_1_joint");
  goal.trajectory.joint_names.push_back("arm_2_joint");
  goal.trajectory.joint_names.push_back("arm_3_joint");    //tf::Transform base_to_gripper;
    //tf::pointMsgToTF(p_in, p_in_tf);
    //ros::Time now = ros::Time::now();
    //tf::StampedTransform time_transform;
    //listener_.waitForTransform("gripper_grasping_frame", "base_link", now, ros::Duration(3.0));
    //listener_.lookupTransform("gripper_grasping_frame", "base_link", now, base_to_gripper);
  goal.trajectory.joint_names.push_back("arm_4_joint");
  goal.trajectory.joint_names.push_back("arm_5_joint");
  goal.trajectory.joint_names.push_back("arm_6_joint");
  goal.trajectory.joint_names.push_back("arm_7_joint");

  // Two waypoints in this goal trajectory
  goal.trajectory.points.resize(1);

  // First trajectory point
  // Positions
  int index = 0;
  goal.trajectory.points[index].positions.resize(7);
  goal.trajectory.points[index].positions[0] = joint_d(0);
  goal.trajectory.points[index].positions[1] = joint_d(1);
  goal.trajectory.points[index].positions[2] = joint_d(2);
  goal.trajectory.points[index].positions[3] = joint_d(3);
  goal.trajectory.points[index].positions[4] = joint_d(4);
  goal.trajectory.points[index].positions[5] = joint_d(5);
  goal.trajectory.points[index].positions[6] = joint_d(6);

  // Velocities
  goal.trajectory.points[index].velocities.resize(7);
  for (int j = 0; j < 7; ++j)
  {
    goal.trajectory.points[index].velocities[j] = 0.0;
  }
  // To be reached 0.1 second after starting along the trajectory
  goal.trajectory.points[index].time_from_start = ros::Duration(0.02);
}

// Generates a simple trajectory with two waypoints to move TIAGo's arm
void Cartesian::waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, MatrixXd desired_joint_pose, int num_sub_path)
{
  // The joint names, which apply to all waypoints
  goal.trajectory.joint_names.push_back("arm_1_joint");
  goal.trajectory.joint_names.push_back("arm_2_joint");
  goal.trajectory.joint_names.push_back("arm_3_joint");
  goal.trajectory.joint_names.push_back("arm_4_joint");
  goal.trajectory.joint_names.push_back("arm_5_joint");
  goal.trajectory.joint_names.push_back("arm_6_joint");
  goal.trajectory.joint_names.push_back("arm_7_joint");

  // num_sub_path waypoints in this goal trajectory
  goal.trajectory.points.resize(num_sub_path);
  
  int index = 0;
  for (int i = 0; i < num_sub_path; i++)
  {
    // Positions
  	goal.trajectory.points[index].positions.resize(7);
    goal.trajectory.points[index].positions[0] = desired_joint_pose(0, index);//   desired_joint_pose[0][index];
    goal.trajectory.points[index].positions[1] = desired_joint_pose(1, index);// [1][index];
    goal.trajectory.points[index].positions[2] = desired_joint_pose(2, index);// [2][index];
    goal.trajectory.points[index].positions[3] = desired_joint_pose(3, index);// [3][index];
    goal.trajectory.points[index].positions[4] = desired_joint_pose(4, index);// [4][index];
    goal.trajectory.points[index].positions[5] = desired_joint_pose(5, index);// [5][index];
    goal.trajectory.points[index].positions[6] = desired_joint_pose(6, index);// [6][index];

    ROS_INFO("joint_pose is: %f", goal.trajectory.points[index].positions[0]);
    
    // Velocities
    goal.trajectory.points[index].velocities.resize(7);
    for (int j = 0; j < 7; ++j)
    {
      goal.trajectory.points[index].velocities[j] = 0.0;
    }
    // To be reached specialized seconds after starting along the trajectory
    goal.trajectory.points[index].time_from_start = ros::Duration(2.0 * (index + 1));

    index += 1;
  }
}

// end of left finger
void Cartesian::patch1_callback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch)
{
    if(patch->data.at(0).cellId == 7)
    {
        patch1.force = max( max( patch->data.at(0).force.at(0), patch->data.at(0).force.at(1) ) , patch->data.at(0).force.at(2) );
        patch1.prox = patch->data.at(0).prox.at(0);
        patch1.temp =  patch->data.at(0).temp.at(0);

    }
}
// end of right finger (available)
void Cartesian::patch2_callback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch)
{
    if(patch->data.at(0).cellId == 35)
    {
        patch2.force = max( max( patch->data.at(0).force.at(0), patch->data.at(0).force.at(1) ) , patch->data.at(0).force.at(2) );
        patch2.prox = patch->data.at(0).prox.at(0);
        patch2.temp =  patch->data.at(0).temp.at(0);

    }
}

// Entry point
int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "run_traj_control");
  ros::AsyncSpinner spinner(4);
  spinner.start();
  ros::NodeHandle nh;
  Cartesian node(nh);


  ros::waitForShutdown();
  return 0;

}
