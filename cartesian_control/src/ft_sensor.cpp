#include <ros/ros.h>
//#include <geometry_msgs/WrenchStamped.h>

//#include <stdio.h>
//#include <vector>

// C++ library
#include <stdio.h>
#include <iostream>
#include <string>
#include <cmath>

/*
// vectors for the measured forces and torques from sensor
std::vector<double> force(3);
std::vector<double> torque(3);

//double force;

void ftSensorCb(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{
  force[0] = msg->wrench.force.x;
  force[1] = msg->wrench.force.y;
  force[2] = msg->wrench.force.z;

  //torque[0] = msg->wrench.torque.x;
  //torque[1] = msg->wrench.torque.y;
  //torque[2] = msg->wrench.torque.z;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ft_sensor");

  ros::NodeHandle n;

  ros::Subscriber tf_sensor_sub = n.subscribe("/wrist_ft", 1, ftSensorCb);

  ros::Rate loop_rate = 10;

  while(ros::ok())
  {
  	ROS_INFO("*****************");

  	ROS_INFO("Force is: %f", force[2]);
  	ros::spinOnce();

  	loop_rate.sleep();
  }

}
*/

const unsigned int num_sub_path = 5;
double desired_pose [6][num_sub_path] = {0};

double cartesian_vmax = 0.05;

double my_array[num_sub_path] = {0};
double *d_pose = my_array;

// declare all functions
double create_trajectory(double t, double vmax, double d);
void generate_trajectory(double vmax, double pose_diff, int num_interpolation);


void generate_trajectory(double vmax, double pose_diff, int num_interpolation, double *d_pose)
{
  // generate a trapezoidal motion path
  double t_from_start = 0;
  double t_end = 4 * pose_diff / 3 / vmax;
  double delta_t = (t_end - t_from_start) / num_interpolation;
 
  for (int index = 0; index < num_interpolation; index++)
  {
    t_from_start = delta_t * (index + 1);
    *(d_pose + index) = create_trajectory(t_from_start, vmax, pose_diff);
  }
  
  //return d_pose;
}

double create_trajectory(double t, double vmax, double d)
{
  double trajectory = 0;
  
  // first phase: acceleration
  if (t < d / 3 / vmax)
    trajectory = 3 * pow(vmax * t, 2) / 2 / d;  
  // second phase: constant speed
  else if (t < d / vmax)
    trajectory = t * vmax - d / 6;
  // third phase: deceleration
  else
    trajectory = d - 3 / 2 / d * pow(vmax * (4 * d / 3 / vmax - t), 2);
  return trajectory;
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "test_traj");

  ros::NodeHandle nh;

  double x_init = 1.0, y_init = 0.0, z_init = 0.0, rx_init = 0.0, ry_init = 0.0, rz_init = 0.0;

  double x_d, y_d, z_d, rx_d, ry_d, rz_d;
  x_d = x_init + 0.2;
  y_d = y_init + 0.1;
  z_d = z_init + 0.05;
  rx_d = rx_init;
  ry_d = ry_init;
  rz_d = rz_init;

  // obtain the largest difference among x/y/z axis
  double max_p_diff = (fabs(x_d - x_init) > fabs(y_d - y_init)) ? (x_d - x_init) : (y_d - y_init);
  int max_index = (fabs(x_d - x_init) > fabs(y_d - y_init)) ? 0 : 1;
  max_p_diff = (max_p_diff > fabs(z_d - z_init)) ? max_p_diff : (z_d - z_init);
  max_index = (max_p_diff > fabs(z_d - z_init)) ? max_index : 2;

  double t_duration = 4 * max_p_diff / 3 / cartesian_vmax;

  generate_trajectory(cartesian_vmax, max_p_diff, num_sub_path, d_pose);

  ROS_INFO_STREAM(d_pose[0] << d_pose[1] << d_pose[2]  << d_pose[3] << d_pose[4]);

   // generate the desired trajectory for the direction with largest error
  //double (*p_traj)[num_sub_path] = generate_trajectory(cartesian_vmax, max_p_diff, num_sub_path);
  /*
  double (*x_traj)[num_sub_path];
  double (*y_traj)[num_sub_path];
  double (*z_traj)[num_sub_path];

  switch (max_index)
  {
    case 0: // x_error largest
      {
        x_traj = p_traj;
        for (int sub_path = 0; sub_path < num_sub_path; sub_path++)
          desired_pose[0][sub_path] = *x_traj[sub_path];

        y_traj = generate_trajectory(4 * (y_d - y_init) / 3 / t_duration, y_d - y_init, num_sub_path);
        for (int sub_path = 0; sub_path < num_sub_path; sub_path++)
          desired_pose[1][sub_path] = *y_traj[sub_path];     

        z_traj = generate_trajectory(4 * (z_d - z_init) / 3 / t_duration, z_d - z_init, num_sub_path);
        for (int sub_path = 0; sub_path < num_sub_path; sub_path++)
          desired_pose[2][sub_path] = *z_traj[sub_path];
      }
      break;
    case 1: // y_error largest
      {
        y_traj = p_traj;
        for (int sub_path = 0; sub_path < num_sub_path; sub_path++)
          desired_pose[1][sub_path] = *y_traj[sub_path];

        x_traj = generate_trajectory(4 * (x_d - x_init) / 3 / t_duration, x_d - x_init, num_sub_path);
        for (int sub_path = 0; sub_path < num_sub_path; sub_path++)
          desired_pose[0][sub_path] = *x_traj[sub_path];

        z_traj = generate_trajectory(4 * (z_d - z_init) / 3 / t_duration, z_d - z_init, num_sub_path);
        for (int sub_path = 0; sub_path < num_sub_path; sub_path++)
          desired_pose[2][sub_path] = *z_traj[sub_path];
      }
      break;
    case 2: // z_error largest
      {
        z_traj = p_traj;
        for (int sub_path = 0; sub_path < num_sub_path; sub_path++)
          desired_pose[2][sub_path] = *z_traj[sub_path];

        x_traj = generate_trajectory(4 * (x_d - x_init) / 3 / t_duration, x_d - x_init, num_sub_path);
        for (int sub_path = 0; sub_path < num_sub_path; sub_path++)
          desired_pose[0][sub_path] = *x_traj[sub_path];

        y_traj = generate_trajectory(4 * (y_d - y_init) / 3 / t_duration, y_d - y_init, num_sub_path);
        for (int sub_path = 0; sub_path < num_sub_path; sub_path++)
          desired_pose[1][sub_path] = *y_traj[sub_path];
      }
  }

  ROS_INFO_STREAM(desired_pose[0][0] << desired_pose[0][1] << desired_pose[0][2]  << desired_pose[0][3] << desired_pose[0][4] << desired_pose[0][5] << desired_pose[0][6] << desired_pose[0][7] << desired_pose[0][8] << desired_pose[0][9]);
  ROS_INFO_STREAM(desired_pose[1][0] << desired_pose[1][1] << desired_pose[1][2]  << desired_pose[1][3] << desired_pose[1][4]);
  ROS_INFO_STREAM(desired_pose[2][0] << desired_pose[2][1] << desired_pose[2][2]  << desired_pose[2][3] << desired_pose[2][4]);
*/
return EXIT_SUCCESS;

}