// ros library
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <std_srvs/Empty.h>
#include <tum_ics_skin_msgs/SkinCellDataArray.h>
#include <geometry_msgs/WrenchStamped.h>
#include <control_msgs/JointTrajectoryControllerState.h>
// C++ library
#include <stdio.h>
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <Eigen/Core>


using namespace std;
using namespace Eigen;

class Gripper_controll
{
private:
    //-------------parameters---------------//
    double k_p_g, stiffness_g, gripper_effort_expected, object_width, g_offset, dt;
    Matrix<double, 2, 1> gripper_position;
    Matrix<double, 2, 1> gripper_effort;
    ros::NodeHandle nh_, priv_nh_;
    ros::Subscriber gripper_force_sub, gripper_position_sub, patch1_sub, patch3_sub, patch5_sub, wrist_ft_sub, patch9_sub;
    ros::Publisher gripper_position_pub;
    ros::ServiceServer gripper_srv, skinGripper_srv, skinGripperOff_srv, gripper_off;
    double force_z;
    double force_thrh6, force_thrh5, force_thrh8, force_thrh34, force_thrh33, force_thrh31;
    double prox_thrh6, prox_thrh5, prox_thrh8, prox_thrh34, prox_thrh33, prox_thrh31;
    double GRight_position, GLeft_position;
    double right_prox, left_prox, right_force, left_force;

    bool PLeft_in, PRight_in, FLeft_in, FRight_in;
    //-------------struct-----------------//
    struct Patch
    {
        double force;
        double prox;
        double temp;
    };
    Patch patch1_1, patch3_6, patch3_8, patch3_5, patch5_34, patch5_33, patch5_31, patch9;
    //-------------callback-------------------//
    void jointStateCb(const sensor_msgs::JointState::ConstPtr& msg);
    void patch9_calback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch);
    void patch3_calback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch);
    void patch5_calback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch);
    void gripper_callback(const control_msgs::JointTrajectoryControllerStateConstPtr& state);
    //void patch3_calback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch);
    void wristTf_calback(const geometry_msgs::WrenchStamped::ConstPtr& msg);
    double gripper_control(double p_error, double f_error, double k_p, double s);
    bool gripper_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    bool skinGripper_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    bool skinGripperOff_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    bool offGripper_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);

public:
    Gripper_controll(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
    {
        k_p_g = 5.0;
        stiffness_g = 20.0;
        gripper_effort_expected = -0.3;
        object_width = 0.05;
        g_offset = 0.005;
        dt = 0.01;
        PLeft_in = false;
        PRight_in = false;
        FLeft_in = false;
        FRight_in = false;
        gripper_force_sub = nh_.subscribe("joint_states", 1, &Gripper_controll::jointStateCb, this);
        gripper_position_pub = nh_.advertise<trajectory_msgs::JointTrajectory>("gripper_controller/command", 2);
        //patch1_sub = nh_.subscribe("/tiago/patch2", 1, &Gripper_controll::patch1_calback, this);
        patch3_sub = nh_.subscribe("/tiago/patch3", 1, &Gripper_controll::patch3_calback, this);
        patch5_sub = nh_.subscribe("/tiago/patch5", 1, &Gripper_controll::patch5_calback, this);
        gripper_position_sub = nh_.subscribe("/gripper_controller/state", 1, &Gripper_controll::gripper_callback, this);

        patch9_sub = nh_.subscribe("/tiago/patch9", 1, &Gripper_controll::patch9_calback, this);
        wrist_ft_sub = nh_.subscribe("/wrist_ft", 1, &Gripper_controll::wristTf_calback, this);
        gripper_srv = nh_.advertiseService("/gripper_service", &Gripper_controll::gripper_srv_callback, this);
        skinGripper_srv = nh_.advertiseService("/skinGripper_service", &Gripper_controll::skinGripper_srv_callback, this);
        skinGripperOff_srv = nh_.advertiseService("/skinGripperOff_service", &Gripper_controll::skinGripperOff_srv_callback, this);
        gripper_off = nh_.advertiseService("/off_gripper", &Gripper_controll::offGripper_srv_callback, this);
    }
    ~Gripper_controll(){}
};

bool Gripper_controll::offGripper_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ros::Rate loop_rate = 50;
    ros::Rate loop_r = 2;
    ros::Duration running_time(0.5);
    ros::Duration openning_time(3.0);

    trajectory_msgs::JointTrajectory g_msg;
    trajectory_msgs::JointTrajectoryPoint g_p_msg;

    g_p_msg.positions.push_back(0);
    g_p_msg.positions.push_back(0);

    ROS_INFO("Number of elements: %d", g_p_msg.positions.size());

    g_msg.joint_names.push_back("gripper_left_finger_joint");
    g_msg.joint_names.push_back("gripper_right_finger_joint");

    g_p_msg.time_from_start = openning_time;
    g_p_msg.positions[0] = 0.04;//right gripper
    g_p_msg.positions[1] = 0.04;//left gripper
    g_msg.points.push_back(g_p_msg);

    ros::Duration(1.0).sleep();

    //close the grippers 5 seconds
    ros::Time begin = ros::Time::now();
    ros::Time end = ros::Time::now();

    while(ros::ok() && (end - begin).toSec() < 5)
    {
      gripper_position_pub.publish(g_msg);
      ros::spinOnce();
      loop_rate.sleep();
      end = ros::Time::now();
    }
    return true;


}
bool Gripper_controll::skinGripperOff_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{

    while(patch9.prox <= 0.3 && patch9.force <= 0.1)
    {
        ROS_INFO("Waiting for signal");
    }

    ros::Rate loop_rate = 50;
    ros::Rate loop_r = 2;
    ros::Duration running_time(0.5);
    ros::Duration openning_time(3.0);

    trajectory_msgs::JointTrajectory g_msg, g_msgs1;
    trajectory_msgs::JointTrajectoryPoint g_p_msg;

    g_p_msg.positions.push_back(0);
    g_p_msg.positions.push_back(0);

    ROS_INFO("Number of elements: %d", g_p_msg.positions.size());

    g_msg.joint_names.push_back("gripper_left_finger_joint");
    g_msg.joint_names.push_back("gripper_right_finger_joint");
    g_msgs1.joint_names.push_back("gripper_left_finger_joint");
    g_msgs1.joint_names.push_back("gripper_right_finger_joint");    
    g_p_msg.time_from_start = openning_time;
    g_p_msg.positions[0] = 0.04;//right gripper
    g_p_msg.positions[1] = 0.04;//left gripper
    g_msg.points.push_back(g_p_msg);

    ros::Duration(2.0).sleep();

    //close the grippers 5 seconds
    ros::Time begin = ros::Time::now();
    ros::Time end = ros::Time::now();

    while(ros::ok() && (end - begin).toSec() < 5)
    {
        //ROS_INFO("Position of gripper left is: %f", gripper_position[0]);
        //ROS_INFO("Position of gripper right is: %f", gripper_position[1]);

        //ROS_INFO("Effort of gripper left is: %f", gripper_effort[0]);
        //ROS_INFO("Effort of gripper right is: %f", gripper_effort[1]);

        gripper_position_pub.publish(g_msg);
            ros::spinOnce();
        loop_rate.sleep();
        end = ros::Time::now();
    }


    ros::Duration(3.0).sleep();
    g_p_msg.time_from_start = openning_time;
    g_p_msg.positions[0] = 0.0;//right gripper
    g_p_msg.positions[1] = 0.0;//left gripper
    g_msgs1.points.push_back(g_p_msg);

    ros::Duration(2.0).sleep();

    //close the grippers 5 seconds
    ros::Time begin2 = ros::Time::now();
    ros::Time end2 = ros::Time::now();

    while(ros::ok() && (end2 - begin2).toSec() < 5)
    {
        //ROS_INFO("Position of gripper left is: %f", gripper_position[0]);
        //ROS_INFO("Position of gripper right is: %f", gripper_position[1]);

        //ROS_INFO("Effort of gripper left is: %f", gripper_effort[0]);
        //ROS_INFO("Effort of gripper right is: %f", gripper_effort[1]);

        gripper_position_pub.publish(g_msgs1);
            ros::spinOnce();
        loop_rate.sleep();
        end2 = ros::Time::now();
    }



    return true;
}

    
void Gripper_controll::gripper_callback(const control_msgs::JointTrajectoryControllerStateConstPtr& state)
{
    GRight_position = state->desired.positions[0];
    GLeft_position = state->desired.positions[1];
    //ROS_INFO_STREAM("gripper_right_position: " << state->desired.positions[0]);
    //ROS_INFO_STREAM("gripper_left_position : " << state->desired.positions[1]);

}

void Gripper_controll::wristTf_calback(const geometry_msgs::WrenchStamped::ConstPtr& msg)
{
    force_z = msg->wrench.force.z +10.0;
}

// information from the end of the right finger
void Gripper_controll::patch9_calback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch)
{
    if(patch->data.at(0).cellId == 40 || patch->data.at(0).cellId ==  45 || patch->data.at(0).cellId == 29 || 
    patch->data.at(0).cellId == 38 || patch->data.at(0).cellId == 41 || patch->data.at(0).cellId == 30 || patch->data.at(0).cellId == 43 ||
    patch->data.at(0).cellId == 47 || patch->data.at(0).cellId == 28 || patch->data.at(0).cellId == 42 || patch->data.at(0).cellId == 39 ||
    patch->data.at(0).cellId == 44 || patch->data.at(0).cellId == 46 || patch->data.at(0).cellId == 27)
    {
        patch9.force = max( max(patch->data.at(0).force.at(0), patch->data.at(0).force.at(1)) , patch->data.at(0).force.at(1) );
        patch9.prox = patch->data.at(0).prox.at(0);
        patch9.temp = patch->data.at(0).temp.at(0);
        //ROS_INFO_STREAM("patch9 prox: " << patch9.prox);
        //ROS_INFO_STREAM("patch9 force: " << patch9.force);

    }

}


void Gripper_controll::patch3_calback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch)
{
    // 6 8 5 11
    if(patch->data.at(0).cellId == 6)
    {
        patch3_6.force = max( max(patch->data.at(0).force.at(0), patch->data.at(0).force.at(1)) , patch->data.at(0).force.at(1) );
        patch3_6.prox = patch->data.at(0).prox.at(0);
        patch3_6.temp = patch->data.at(0).temp.at(0);
    }
    if(patch->data.at(0).cellId == 8)
    {
        patch3_8.force = max( max(patch->data.at(0).force.at(0), patch->data.at(0).force.at(1)) , patch->data.at(0).force.at(1) );
        patch3_8.prox = patch->data.at(0).prox.at(0);
        patch3_8.temp = patch->data.at(0).temp.at(0);
    }
    if(patch->data.at(0).cellId == 5)
    {
        patch3_5.force = max( max(patch->data.at(0).force.at(0), patch->data.at(0).force.at(1)) , patch->data.at(0).force.at(1) );
        patch3_5.prox = patch->data.at(0).prox.at(0);
        patch3_5.temp = patch->data.at(0).temp.at(0);
    }
    if( patch3_6.prox >=  prox_thrh6 || patch3_8.prox >=  prox_thrh8 || patch3_5.prox >=  prox_thrh5)
    {
        PLeft_in = true;
    }

    if( patch3_6.force >=  force_thrh6 || patch3_8.force >=  force_thrh8 || patch3_5.force >=  force_thrh5)
    {
        FLeft_in = true;
    }


}
void Gripper_controll::patch5_calback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch)
{
    // 34 33 31
    if(patch->data.at(0).cellId == 34)
    {
        patch5_34.force = max( max(patch->data.at(0).force.at(0), patch->data.at(0).force.at(1)) , patch->data.at(0).force.at(1) );
        patch5_34.prox = patch->data.at(0).prox.at(0);
        patch5_34.temp = patch->data.at(0).temp.at(0);
    }
    if(patch->data.at(0).cellId == 33)
    {
        patch5_33.force = max( max(patch->data.at(0).force.at(0), patch->data.at(0).force.at(1)) , patch->data.at(0).force.at(1) );
        patch5_33.prox = patch->data.at(0).prox.at(0);
        patch5_33.temp = patch->data.at(0).temp.at(0);
    }
    if(patch->data.at(0).cellId == 31)
    {
        patch5_31.force = max( max(patch->data.at(0).force.at(0), patch->data.at(0).force.at(1)) , patch->data.at(0).force.at(1) );
        patch5_31.prox = patch->data.at(0).prox.at(0);
        patch5_31.temp = patch->data.at(0).temp.at(0);
    }
    if( patch5_34.prox >=  prox_thrh34 || patch5_33.prox >=  prox_thrh33 || patch5_31.prox >=  prox_thrh31)
    {
        PRight_in = true;
    }
    if( patch5_34.force >=  force_thrh34 || patch5_33.force >=  force_thrh33 || patch5_31.force >=  force_thrh31)
    {
        FRight_in = true;
    }




}

/*
void Gripper_controll::patch5_calback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch)
{
    if(patch->data.at(0).cellId == 34)
    {
        //ROS_INFO_STREAM("cellId: " << patch->data.at(0).cellId);
        //ROS_INFO_STREAM("prox6: " << max( max(patch->data.at(0).force.at(0), patch->data.at(0).force.at(1)) , patch->data.at(0).force.at(1) ));
    }
}


void Gripper_controll::patch3_calback(const tum_ics_skin_msgs::SkinCellDataArrayConstPtr& patch)
{
    if(patch->data.at(0).cellId == 6)
    {
        ROS_INFO_STREAM("cellId: " << patch->data.at(0).cellId);
        ROS_INFO_STREAM("prox6: " << max( max(patch->data.at(0).force.at(0), patch->data.at(0).force.at(1)) , patch->data.at(0).force.at(1) ));
    }
}*/



void Gripper_controll::jointStateCb(const sensor_msgs::JointState::ConstPtr& msg)
{
  vector<string> joint_name = msg->name;
  vector<double> joint_position = msg->position;
  vector<double> joint_effort = msg->effort;

  unsigned int num_joint = joint_name.size();
  
  for (int i = 0; i < num_joint; i ++)
  {
	if (joint_name[i] == "gripper_left_finger_joint"){
	  gripper_position[0] = joint_position[i];
	  gripper_effort[0] = joint_effort[i];
	}
	if (joint_name[i] == "gripper_right_finger_joint"){
	  gripper_position[1] = joint_position[i];
	  gripper_effort[1] = joint_effort[i];
	}
  }

}

// impedance-admittance controller for the gripper
double Gripper_controll::gripper_control(double p_error, double f_error, double k_p, double s)
{
  double dx = - k_p * (p_error + f_error / s);
  return dx;
}


bool Gripper_controll::gripper_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ros::Rate loop_rate = 50;
    ros::Rate loop_r = 10;
    ros::Duration running_time(0.5);
    ros::Duration openning_time(3.0);

    trajectory_msgs::JointTrajectory g_msg;
    trajectory_msgs::JointTrajectoryPoint g_p_msg;

    g_p_msg.positions.push_back(0);
    g_p_msg.positions.push_back(0);

    ROS_INFO("Number of elements: %d", g_p_msg.positions.size());

    g_msg.joint_names.push_back("gripper_left_finger_joint");
    g_msg.joint_names.push_back("gripper_right_finger_joint");

    g_p_msg.time_from_start = openning_time;
    g_p_msg.positions[0] = 0.04;
    g_p_msg.positions[1] = 0.04;
    g_msg.points.push_back(g_p_msg);

    ros::Duration(1.0).sleep();

    //close the grippers 5 seconds
    ros::Time begin = ros::Time::now();
    ros::Time end = ros::Time::now();

    while(ros::ok() && (end - begin).toSec() < 5)
    {
      //ROS_INFO("Position of gripper left is: %f", gripper_position[0]);
      //ROS_INFO("Position of gripper right is: %f", gripper_position[1]);

      //ROS_INFO("Effort of gripper left is: %f", gripper_effort[0]);
      //ROS_INFO("Effort of gripper right is: %f", gripper_effort[1]);

      gripper_position_pub.publish(g_msg);
      ros::spinOnce();
      loop_rate.sleep();
      end = ros::Time::now();
    }

    double g_position_error_left = gripper_position[0] - object_width / 2 - g_offset;
    double g_position_error_right = gripper_position[1] - object_width / 2 - g_offset;

    double g_effort_error_left = -(gripper_effort_expected - gripper_effort[0]); // expected effort points to outside
    double g_effort_error_right = -(gripper_effort_expected - gripper_effort[1]);

    g_p_msg.time_from_start = running_time;

    double x_left = gripper_position[0];
    double x_right = gripper_position[1];

    ros::Duration(1.0).sleep();

    while(ros::ok() && (fabs(g_effort_error_left > 0.08) || fabs(g_effort_error_right > 0.08)))
    {
      ROS_INFO("Hello!");
      // update error

      g_position_error_left = gripper_position[0] - object_width / 2 - g_offset;
      g_position_error_right = gripper_position[1] - object_width / 2 - g_offset;

      g_effort_error_left = -(gripper_effort_expected - gripper_effort[0]); // expected effort points to outside
      g_effort_error_right = -(gripper_effort_expected - gripper_effort[1]);

      ROS_INFO_STREAM("position error: " << g_position_error_left << " " << g_position_error_right << " effort error: " << g_effort_error_left << " " << g_effort_error_right);

      // apply impedance-admittance control strategy
      double x_dot_left = gripper_control(g_position_error_left, g_effort_error_left, k_p_g, stiffness_g);
      double x_dot_right = gripper_control(g_position_error_right, g_effort_error_right, k_p_g, stiffness_g);

      ROS_INFO_STREAM("x_dot: " << x_dot_left << " " << x_dot_right);

      x_left = x_left + x_dot_left * dt;
      x_right = x_right + x_dot_right * dt;

      if (x_left < 0.01)
        x_left = 0.01;
      if (x_right < 0.01)
        x_right = 0.01;

      g_p_msg.positions[0] = x_left;
      g_p_msg.positions[1] = x_right;

      ROS_INFO_STREAM("x: " << x_left << " " << x_right);

      g_msg.points[0] = g_p_msg;

      gripper_position_pub.publish(g_msg);
      loop_r.sleep();
    }

    ROS_INFO("Movement of Gripper is Done!");

    return true;
}


bool Gripper_controll::skinGripper_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ros::Rate loop_rate = 50;
    ros::Rate loop_r = 2;
    ros::Duration running_time(0.5);
    ros::Duration openning_time(3.0);

    trajectory_msgs::JointTrajectory g_msg;
    trajectory_msgs::JointTrajectoryPoint g_p_msg;

    g_p_msg.positions.push_back(0);
    g_p_msg.positions.push_back(0);

    //ROS_INFO("Number of elements: %d", g_p_msg.positions.size());

    g_msg.joint_names.push_back("gripper_left_finger_joint");
    g_msg.joint_names.push_back("gripper_right_finger_joint");

    g_p_msg.time_from_start = openning_time;
    g_p_msg.positions[0] = 0.04;//right gripper
    g_p_msg.positions[1] = 0.04;//left gripper
    g_msg.points.push_back(g_p_msg);

    ros::Duration(1.0).sleep();

    //close the grippers 5 seconds
    ros::Time begin = ros::Time::now();
    ros::Time end = ros::Time::now();

    while(ros::ok() && (end - begin).toSec() < 5)
    {
      //ROS_INFO("Position of gripper left is: %f", gripper_position[0]);
      //ROS_INFO("Position of gripper right is: %f", gripper_position[1]);

      //ROS_INFO("Effort of gripper left is: %f", gripper_effort[0]);
      //ROS_INFO("Effort of gripper right is: %f", gripper_effort[1]);

      gripper_position_pub.publish(g_msg);
      ros::spinOnce();
      loop_rate.sleep();
      end = ros::Time::now();
    }

    g_p_msg.time_from_start = running_time;

    ros::Duration(1.0).sleep();
    while(ros::ok() && ( patch3_6.prox <= 0.17 || patch5_34.prox <= 0.17 ))
    {
        ROS_INFO("Get close !");
        //ROS_INFO_STREAM("Right_34 prox: " << patch5_34.prox << "Left_6 prox " << patch3_6.prox);
        
        right_prox = patch5_34.prox;
        left_prox = patch3_6.prox;
        if(right_prox <= 0.17)
        {
            g_p_msg.positions[0] = GRight_position - 0.01 * (0.17 - right_prox) ;
        }
        else
        {
            g_p_msg.positions[0] = GRight_position;
        }
        if(left_prox <= 0.17)
        {
            g_p_msg.positions[1] = GLeft_position - 0.01 * (0.17 - left_prox);
        }
        else
        {
            g_p_msg.positions[1] = GLeft_position;
        }
        //ROS_INFO_STREAM("gripper left: " << GLeft_position << "gripper right " << GRight_position);
        g_msg.points[0] = g_p_msg;
        gripper_position_pub.publish(g_msg);
        ros::spinOnce();
        loop_r.sleep();
    }

    ros::Duration(2.0).sleep();
    while(ros::ok() && ( patch3_6.force + patch5_34.force <= 0.13 ))
    {
        ROS_INFO("Grasping !");

        ROS_INFO_STREAM("Right_34 force: " << patch5_34.force << "Left_6 force " << patch3_6.force);
        
        right_force = patch5_34.force;
        left_force = patch3_6.force;
        if(right_force <= 0.09)
        {
            g_p_msg.positions[0] = GRight_position - 0.02 * (0.12 - right_force) ;
        }
        else
        {
            g_p_msg.positions[0] = GRight_position;
        }
        if(left_force <= 0.09)
        {
            g_p_msg.positions[1] = GLeft_position - 0.02 * (0.12 - left_force);
        }
        else
        {
            g_p_msg.positions[1] = GLeft_position;
        }

        //ROS_INFO_STREAM("gripper left: " << GLeft_position << "gripper right " << GRight_position);
        g_msg.points[0] = g_p_msg;
        gripper_position_pub.publish(g_msg);
        ros::spinOnce();
        loop_r.sleep();
    }

    ROS_INFO("Movement of Gripper is Done!");

    return true;
}






int main(int argc, char **argv)
{
  ros::init(argc, argv, "gripper_control");
  ros::AsyncSpinner spinner(4);
  spinner.start();
  ros::NodeHandle nh;
  Gripper_controll node(nh);
  ros::waitForShutdown();

  return EXIT_SUCCESS;
}
