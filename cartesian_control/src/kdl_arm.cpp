#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chain.hpp>
#include <kdl/tree.hpp> 
#include <kdl/segment.hpp> 
#include <kdl/chainfksolver.hpp> 
#include <kdl/chainfksolverpos_recursive.hpp> 
#include <kdl/frames_io.hpp> 
#include <stdio.h> 
#include <iostream> 

using namespace KDL; 
using namespace std; 

int main(int argc, char** argv){
  ros::init(argc, argv, "kdl_arm");
  ros::NodeHandle n;

  KDL::Tree my_tree;
  kdl_parser::treeFromFile("/home/atHomeWS18-19/ros/workspace/tiago_ws/src/tiago_robot/tiago_description/urdf/arm/arm.urdf",my_tree);
  bool exit_value;
  Chain chain;
  exit_value = my_tree.getChain("torso_lift_link","arm_tool_link",chain);
  ChainFkSolverPos_recursive fksolver = ChainFkSolverPos_recursive(chain);
  unsigned int nj = chain.getNrOfJoints();
  JntArray jointpositions = JntArray(nj);
  for(unsigned int i=0;i<nj;i++){
    float myinput; printf("Enter the position of joint %i: ",i);
    scanf("%e",&myinput);
    jointpositions(i)=(double)myinput; 
  }
  Frame cartpos; 
  bool kinematics_status;
  kinematics_status = fksolver.JntToCart(jointpositions,cartpos);
  if(kinematics_status>=0){
    std::cout << cartpos << std::endl;
	std::cout << cartpos(0,3) << std::endl;
    printf("%s \n","Success, thanks KDL!");
  }
  else{
    printf("%s \n","Error:could not calculate forward kinematics : ");
  }


}
