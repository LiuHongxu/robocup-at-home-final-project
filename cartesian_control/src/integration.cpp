#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
//
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/move_group_interface/move_group.h>
#include <geometry_msgs/Twist.h>
//service include
#include <cartesian_control/pose_error.h>
#include <cartesian_control/transform.h>
#include <cartesian_control/transform2.h>
#include <cartesian_control/navigation_pose.h>
#include <plane_segmentation/center_points.h>
#include "from2dto3d/object.h"
#include <controller/forward.h>
#include <controller/rotation_test.h>

//
#include <string>
#include <vector>
#include <map>
#include <iomanip>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int main(int argc, char **argv)
{

    enum state_set {doorHandle_seg, pre_grasp, pre_grasp2, pre_graspObject, cartesian_open, again,
                    admittance_open1, cartesian_open2, cartesian_catch, admittance_catch, gripper_grasp, cartesian_catch2, gripper_off, detection, 
                    rotation_1, rotation_2, rotation_3, pre_shakeHands, close_drawer, back_origin, finish};
    state_set state;
    std_srvs::Empty std_empty;
    bool detect_object = false;
	std::string ready;
    from2dto3d::object object;
	controller::forward forward_srv;
    controller::rotation_test rotation_srv;
    ros::init(argc, argv, "integration");
    ros::NodeHandle nh;
    ros::Rate loop_rate(10);
    ros::AsyncSpinner spinner(1);
    spinner.start();
  //tell the action client that we want to spin a thread by default
     MoveBaseClient ac("move_base", true);

  //wait for theaction server to come up
    while (!ac.waitForServer(ros::Duration(5.0))) 
    {
        ROS_INFO("Waiting for the move_base action server to come up");
    }
	//servers from pregrasp nodes
    ros::ServiceClient preGrasHandle_cli = nh.serviceClient<std_srvs::Empty>("/pre_grasp_handle");
    ros::ServiceClient preGrasObject_cli = nh.serviceClient<std_srvs::Empty>("/pre_grasp_object");
    ros::ServiceClient preGrasObject_cli1 = nh.serviceClient<std_srvs::Empty>("/pre_grasp_object1");
    ros::ServiceClient gripperOff_cli = nh.serviceClient<std_srvs::Empty>("/off_gripper");
    ros::ServiceClient home_cli = nh.serviceClient<std_srvs::Empty>("/home");
    ros::ServiceClient downhead_cli = nh.serviceClient<std_srvs::Empty>("/down_head");
    ros::ServiceClient gre_shake_cli = nh.serviceClient<std_srvs::Empty>("/pre_grasp_shake");
	//server from cartesian nose
    ros::ServiceClient transform_cli = nh.serviceClient<cartesian_control::transform>("/transform_service");
    ros::ServiceClient transform2_cli = nh.serviceClient<cartesian_control::transform2>("/transform2_service");
    ros::ServiceClient transform3_cli = nh.serviceClient<cartesian_control::transform2>("/transform3_service");
    ros::ServiceClient cartesian_cli = nh.serviceClient<cartesian_control::pose_error>("/cartesian_service");
    ros::ServiceClient admittance_cli = nh.serviceClient<cartesian_control::pose_error>("/admittance_service");
    ros::ServiceClient shakeHands_cli = nh.serviceClient<std_srvs::Empty>("/shake_hands_service");
	//server from perception and from2dto3d nodes
    ros::ServiceClient doorHandle_cli = nh.serviceClient<plane_segmentation::center_points>("/DoorHandle");
    ros::ServiceClient objectDet_cli = nh.serviceClient<from2dto3d::object> ("/object_class");
	//server from skin_gripper node
    ros::ServiceClient gripperGrp_cli = nh.serviceClient<std_srvs::Empty>("/skinGripper_service");
    ros::ServiceClient skinOffgripper_cli = nh.serviceClient<std_srvs::Empty>("/skinGripperOff_service");
	//server from mobile base node
    ros::ServiceClient forward_cli = nh.serviceClient<controller::forward>("/forward");
    ros::ServiceClient rotation_cli = nh.serviceClient<controller::rotation_test>("/rotation");

  //service messages declare
    geometry_msgs::Twist vel_msg;
    std_srvs::Empty srv_empty;
    cartesian_control::navigation_pose navigation_srv;
    cartesian_control::pose_error admittance_srv, cartesian_srv;
    cartesian_control::transform transform_srv;
    cartesian_control::transform2 transform2_srv;
    plane_segmentation::center_points doorHandle_srv;
    ros::Duration(10).sleep();
    int count = 0;
    state = doorHandle_seg;

    while(ros::ok())
    {
        switch(state)
        {

            case doorHandle_seg:
                //Door handle segmentation case
                ROS_INFO("doorHandle_seg case");
				downhead_cli.call(std_empty);
                ros::Duration(3.0).sleep();	
                doorHandle_srv.request.find = true;
                if(doorHandle_cli.call(doorHandle_srv))
                {
                    transform_srv.request.x_in = doorHandle_srv.response.x;
                    transform_srv.request.y_in = doorHandle_srv.response.y;
                    transform_srv.request.z_in = doorHandle_srv.response.z;
					ROS_INFO_STREAM("x: " << doorHandle_srv.response.x << ", y: " << doorHandle_srv.response.y << ", z: " << doorHandle_srv.response.z);
                    state = pre_grasp;
                    ROS_INFO("Get the position of the handle successfully !");
                    break;
                }

            case pre_grasp:
                ROS_INFO("pre_grasp case");
                if(preGrasHandle_cli.call(srv_empty))
                {
                    state = cartesian_open;
                    ros::Duration(15.0).sleep();
                    break;
                }

            case cartesian_open:
                ROS_INFO("cartesian_open case");
                ros::Duration(5.0).sleep();
                if(transform_cli.call(transform_srv))
                {
                    cartesian_srv.request.x_error = transform_srv.response.x_out - 0.15;
                    if(transform_srv.response.z_out != 0)
                    {
                        cartesian_srv.request.y_error = - transform_srv.response.z_out;
                    }
                    else
                    {
                        cartesian_srv.request.y_error = 0.000001;
                    }
                    if(transform_srv.response.y_out != 0)
                    {
                        cartesian_srv.request.z_error =  transform_srv.response.y_out + 0.06;
                    }
                    else
                    {
                        cartesian_srv.request.z_error = 0.000001;
                    }
					ROS_INFO_STREAM("x: " << cartesian_srv.request.x_error << ", y: " << cartesian_srv.request.y_error << ", z: " << cartesian_srv.request.z_error);

                }
                if(cartesian_cli.call(cartesian_srv))
                {
                    if(cartesian_srv.response.done)
                    {
                        state = admittance_open1;
                        ros::Duration(2.0).sleep();
                        break;
                    }
                }
            case admittance_open1:
                ROS_INFO("admittance_open1 case");                
                admittance_srv.request.x_error = 0.15;
                admittance_srv.request.y_error = 0.0;
                admittance_srv.request.z_error = 0;
                admittance_srv.request.kp_d = 5;
                admittance_srv.request.pregrasp = false;
                
                if(admittance_cli.call(admittance_srv))
                {
                    if(admittance_srv.response.done)
                    {
                        state = gripper_grasp;
                        ros::Duration(2.0).sleep();
                        break;
                    }
                }
            case gripper_grasp:
                ROS_INFO("gripper_grasp case");                
                if(gripperGrp_cli.call(std_empty))
                {
                    state = cartesian_open2;
                    ros::Duration(2.0).sleep();
                    break;
                }
            case cartesian_open2:
                ROS_INFO("cartesian_open2 case");                
                ROS_INFO("open the drawer");
                cartesian_srv.request.x_error = - 0.2;
                cartesian_srv.request.y_error = 0.0000001;
                cartesian_srv.request.z_error = 0.0000001;
                if(cartesian_cli.call(cartesian_srv))
                {
                    if(cartesian_srv.response.done)
                    {

						forward_srv.request.distance = - 0.15;
						if(forward_cli.call(forward_srv))
						{
							ros::Duration(2.0).sleep();
                        	if(gripperOff_cli.call(std_empty))
                        	{
                            	cartesian_srv.request.x_error = - 0.05; 
                            	cartesian_srv.request.y_error = 0.0000001;
                            	cartesian_srv.request.z_error = 0.0000001;
                            	cartesian_cli.call(cartesian_srv);
                            	ros::Duration(2.0).sleep();
                            	state = rotation_1;
                            	break;
                        	}
						}                        
                    }
                }
            case rotation_1:
				rotation_srv.request.angle = 1.5708;//0.5 *pi
				if(rotation_cli.call(rotation_srv))
				{
                    home_cli.call(std_empty);
                    ros::Duration(4.0).sleep();
					//state = pre_shakeHands;
                    //break;
                    
                    
                    rotation_srv.request.angle = -1.5708;
					if(rotation_cli.call(rotation_srv))
					{
						forward_srv.request.distance = 0.47;
						if(forward_cli.call(forward_srv))
						{
						    state = detection;
						    break;        
						}
					}
				}

            case detection:
                ros::Duration(5.0).sleep();
                preGrasObject_cli1.call(std_empty);
                ros::Duration(6.0).sleep();
                ROS_INFO("detection case");
/////              speach server
				object.request.object_class = "apple";
                while(!detect_object)
                {
                    if(objectDet_cli.call(object))
                    {
                        if(transform3_cli.call(transform2_srv)) //transform from camera to base_link
                        {
							//ROS_INFO_STREAM(__LINE__);
							//ROS_INFO_STREAM("x_base: " << transform2_srv.response.x_out << "y_base: " << transform2_srv.response.y_out << "z_base: " << transform2_srv.response.z_out);


                            if( 0.5 < transform2_srv.response.x_out  && transform2_srv.response.x_out < 0.9 && fabs(transform2_srv.response.y_out) < 0.3 &&  0.60 < transform2_srv.response.z_out && transform2_srv.response.z_out< 0.70)
                            {
                                detect_object = true;
                            }
                            else
                            {
                                ROS_INFO("Cannot see the apple please move it a little bit");
                            }
                            
                        }
                    }
                }
                state = pre_graspObject;
                break;

			case pre_graspObject:
                preGrasObject_cli.call(std_empty);
                ros::Duration(5.0).sleep();
                transform2_cli.call(transform2_srv);
                admittance_srv.request.x_error = - transform2_srv.response.z_out + 0.02;
                admittance_srv.request.y_error = - transform2_srv.response.y_out;
                admittance_srv.request.z_error = - transform2_srv.response.x_out + 0.06;
                admittance_srv.request.kp_d = 1.0;
				admittance_srv.request.pregrasp = true;
				ROS_INFO_STREAM("x " << admittance_srv.request.x_error << "y " << admittance_srv.request.y_error << "z" << admittance_srv.request.z_error);
                if(fabs(admittance_srv.request.x_error) > 0.2 ||  fabs(admittance_srv.request.y_error > 0.3) || fabs(admittance_srv.request.z_error > 0.2))
				{
                    state = again;
					break;
				}
                else
                {
                    state = cartesian_catch;
                    break;
                }
            case again:
                cartesian_srv.request.x_error = 0.0000001;
                cartesian_srv.request.y_error = 0.0000001;
                cartesian_srv.request.z_error = 0.05;

                if(cartesian_cli.call(cartesian_srv))
                {
    				rotation_srv.request.angle = 3.1416;//0.5 *pi
	    			if(rotation_cli.call(rotation_srv))
                    {
                        home_cli.call(std_empty);
                        ros::Duration(5.0).sleep();
				        rotation_srv.request.angle = -3.1416;//0.5 *pi
                        if(rotation_cli.call(rotation_srv))
                        {
                            detect_object = false;
                            state = detection;
                            break;
                        }
                    }
                }



			case cartesian_catch:
                ROS_INFO("cartesian_catch case");
				ROS_INFO_STREAM("x_adm: " << admittance_srv.request.x_error << "y_adm: " << admittance_srv.request.y_error << "z_adm: " << admittance_srv.request.z_error);
				std::cout<<("$: ready ?");
				std::cin>>ready;
				ROS_INFO_STREAM("" << ready);
                cartesian_srv.request.x_error = admittance_srv.request.x_error;
                cartesian_srv.request.y_error = admittance_srv.request.y_error;
                cartesian_srv.request.z_error = 0.000001;
                if(cartesian_cli.call(cartesian_srv))
                {
                    state = admittance_catch;
					admittance_srv.request.x_error = 0.0;
					admittance_srv.request.y_error = 0.0;
                    ros::Duration(2.0).sleep();
                    break;
                }


            case admittance_catch:
                ROS_INFO("admittance_catch case");
                if(admittance_cli.call(admittance_srv))
                {
                    ros::Duration(2.0).sleep();
                    if(gripperGrp_cli.call(std_empty))
                    {
                        state = cartesian_catch2;
                    	ros::Duration(5.0).sleep();
                        break;
                    }

                }
            case cartesian_catch2:
                ROS_INFO("cartesian_catch2 case");
                cartesian_srv.request.x_error = 0.0000001;
                cartesian_srv.request.y_error = 0.0000001;
                cartesian_srv.request.z_error = 0.15;
                if(cartesian_cli.call(cartesian_srv))
                {
                    state = rotation_2;
                    ros::Duration(2.0).sleep();
                    break;
                }
            case rotation_2:
				rotation_srv.request.angle = 3.1416;//0.5 *pi
				if(rotation_cli.call(rotation_srv))
				{
                	state = pre_shakeHands;
                    break;
				}
            case pre_shakeHands:
                ROS_INFO("pre_shakeHands case");
                ros::Duration(5.0).sleep();
                gre_shake_cli.call(std_empty);
                ROS_INFO("ready for gripper off! ");
                ros::Duration(10.0).sleep();
                if(skinOffgripper_cli.call(std_empty))
                {
                    if(shakeHands_cli.call(std_empty))
                    {
                        home_cli.call(std_empty);
                        gripperOff_cli.call(std_empty);
                        state = rotation_3;
                        break;
                    }
                }
            case rotation_3:
				rotation_srv.request.angle = - 3.1416;
				if(rotation_cli.call(rotation_srv))
				{
					forward_srv.request.distance = - 0.6;					
					if(forward_cli.call(forward_srv))
					{
                        ros::Duration(4.0).sleep();
						state = close_drawer;
					}

				}

            case close_drawer:
                ROS_INFO("close_drawer case");
                if(preGrasHandle_cli.call(srv_empty))
                {
                    ros::Duration(10.0).sleep();
					forward_srv.request.distance = 0.15;	
					forward_cli.call(forward_srv);

                    ros::Duration(3.0).sleep();
                    cartesian_srv.request.x_error = 0.25;
                    cartesian_srv.request.y_error = 0.0000001;
                    cartesian_srv.request.z_error = 0.0000001;
                    if(cartesian_cli.call(cartesian_srv))
                    {

					forward_srv.request.distance = 0.3;	
					forward_cli.call(forward_srv);


                        ros::Duration(4.0).sleep();
                        cartesian_srv.request.x_error = - 0.25;
                        cartesian_srv.request.y_error = 0.0000001;
                        cartesian_srv.request.z_error = 0.0000001;
                        cartesian_cli.call(cartesian_srv);
                        ros::Duration(2.0).sleep();
                        home_cli.call(std_empty);
                        state = finish;
                        break;
                    }
                }                
            case finish:

				ROS_INFO_STREAM(__LINE__);
				ROS_INFO_STREAM("x_base: " << transform2_srv.response.x_out << "y_base: " << transform2_srv.response.y_out << "z_base: " << transform2_srv.response.z_out);
                 state = finish;
                 break;

            default:
                state = doorHandle_seg;
                break;

        }
        ros::spinOnce();
        loop_rate.sleep();
    }
  return 0;
}
