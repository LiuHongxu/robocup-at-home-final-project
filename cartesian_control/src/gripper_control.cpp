// ros library
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <std_srvs/Empty.h>
//#include <control_
//control_msgs/JointTrajectoryControllerState
// C++ library
#include <stdio.h>
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <Eigen/Core>


using namespace std;
using namespace Eigen;

class Gripper_controll
{
private:
    //-------------parameters---------------//
    double k_p_g, stiffness_g, gripper_effort_expected, object_width, g_offset, dt;
    Matrix<double, 2, 1> gripper_position;
    Matrix<double, 2, 1> gripper_effort;
    ros::NodeHandle nh_, priv_nh_;
    ros::Subscriber gripper_force_sub, gripper_position_sub;
    ros::Publisher gripper_position_pub;
    ros::ServiceServer gripper_srv;
    //-------------callback-------------------//
    void jointStateCb(const sensor_msgs::JointState::ConstPtr& msg);
    double gripper_control(double p_error, double f_error, double k_p, double s);
    bool gripper_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
public:
    Gripper_controll(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
    {
        k_p_g = 5.0;
        stiffness_g = 20.0;
        gripper_effort_expected = -0.3;
        object_width = 0.05;
        g_offset = 0.005;
        dt = 0.01;
        gripper_force_sub = nh_.subscribe("joint_states", 1, &Gripper_controll::jointStateCb, this);
        gripper_position_pub = nh_.advertise<trajectory_msgs::JointTrajectory>("gripper_controller/command", 2);
        gripper_srv = nh_.advertiseService("/gripper_service", &Gripper_controll::gripper_srv_callback, this);
    }
    ~Gripper_controll(){}
};


void Gripper_controll::jointStateCb(const sensor_msgs::JointState::ConstPtr& msg)
{
  vector<string> joint_name = msg->name;
  vector<double> joint_position = msg->position;
  vector<double> joint_effort = msg->effort;

  unsigned int num_joint = joint_name.size();
  
  for (int i = 0; i < num_joint; i ++)
  {
	if (joint_name[i] == "gripper_left_finger_joint"){
	  gripper_position[0] = joint_position[i];
	  gripper_effort[0] = joint_effort[i];
	}
	if (joint_name[i] == "gripper_right_finger_joint"){
	  gripper_position[1] = joint_position[i];
	  gripper_effort[1] = joint_effort[i];
	}
  }

}

// impedance-admittance controller for the gripper
double Gripper_controll::gripper_control(double p_error, double f_error, double k_p, double s)
{
  double dx = - k_p * (p_error + f_error / s);
  
  
  return dx;
}


bool Gripper_controll::gripper_srv_callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
    ros::Rate loop_rate = 50;
    ros::Rate loop_r = 2;
    ros::Duration running_time(0.5);
    ros::Duration openning_time(3.0);

    trajectory_msgs::JointTrajectory g_msg;
    trajectory_msgs::JointTrajectoryPoint g_p_msg;

    g_p_msg.positions.push_back(0);
    g_p_msg.positions.push_back(0);

    ROS_INFO("Number of elements: %d", g_p_msg.positions.size());

    g_msg.joint_names.push_back("gripper_left_finger_joint");
    g_msg.joint_names.push_back("gripper_right_finger_joint");

    g_p_msg.time_from_start = openning_time;
    g_p_msg.positions[0] = 0.04;
    g_p_msg.positions[1] = 0.04;
    g_msg.points.push_back(g_p_msg);

    ros::Duration(1.0).sleep();

    //close the grippers 5 seconds
    ros::Time begin = ros::Time::now();
    ros::Time end = ros::Time::now();

    while(ros::ok() && (end - begin).toSec() < 5)
    {
      ROS_INFO("Position of gripper left is: %f", gripper_position[0]);
      ROS_INFO("Position of gripper right is: %f", gripper_position[1]);

      ROS_INFO("Effort of gripper left is: %f", gripper_effort[0]);
      ROS_INFO("Effort of gripper right is: %f", gripper_effort[1]);

      gripper_position_pub.publish(g_msg);
      ros::spinOnce();
      loop_rate.sleep();
      end = ros::Time::now();
    }

    double g_position_error_left = gripper_position[0] - object_width / 2 - g_offset;
    double g_position_error_right = gripper_position[1] - object_width / 2 - g_offset;

    double g_effort_error_left = -(gripper_effort_expected - gripper_effort[0]); // expected effort points to outside
    double g_effort_error_right = -(gripper_effort_expected - gripper_effort[1]);

    g_p_msg.time_from_start = running_time;

    double x_left = gripper_position[0];
    double x_right = gripper_position[1];

    ros::Duration(1.0).sleep();

    while(ros::ok() && (fabs(g_effort_error_left > 0.08) || fabs(g_effort_error_right > 0.08)))
    {
      ROS_INFO("Hello!");
      // update error

      g_position_error_left = gripper_position[0] - object_width / 2 - g_offset;
      g_position_error_right = gripper_position[1] - object_width / 2 - g_offset;

      g_effort_error_left = -(gripper_effort_expected - gripper_effort[0]); // expected effort points to outside
      g_effort_error_right = -(gripper_effort_expected - gripper_effort[1]);

      ROS_INFO_STREAM("position error: " << g_position_error_left << " " << g_position_error_right << " effort error: " << g_effort_error_left << " " << g_effort_error_right);

      // apply impedance-admittance control strategy
      double x_dot_left = gripper_control(g_position_error_left, g_effort_error_left, k_p_g, stiffness_g);
      double x_dot_right = gripper_control(g_position_error_right, g_effort_error_right, k_p_g, stiffness_g);

      ROS_INFO_STREAM("x_dot: " << x_dot_left << " " << x_dot_right);

      x_left = x_left + x_dot_left * dt;
      x_right = x_right + x_dot_right * dt;

      if (x_left < 0.01)
        x_left = 0.01;
      if (x_right < 0.01)
        x_right = 0.01;

      g_p_msg.positions[0] = x_left;
      g_p_msg.positions[1] = x_right;

      ROS_INFO_STREAM("x: " << x_left << " " << x_right);

      g_msg.points[0] = g_p_msg;

      gripper_position_pub.publish(g_msg);
      loop_r.sleep();
    }

    ROS_INFO("Movement of Gripper is Done!");

    return true;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "gripper_control");
  ros::AsyncSpinner spinner(3);
  spinner.start();
  ros::NodeHandle nh;
  Gripper_controll node(nh);
  ros::waitForShutdown();

  return EXIT_SUCCESS;
}
