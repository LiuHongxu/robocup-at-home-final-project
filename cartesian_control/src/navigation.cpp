#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
//
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/move_group_interface/move_group.h>
#include <cartesian_control/navigation_pose.h>
//
#include <string>
#include <vector>
#include <map>

class Navigation
{
private:
  typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
  typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> joint_control_client;

  //! The node handle
  ros::NodeHandle nh_;
  //! Node handle in the private namespace
  ros::NodeHandle priv_nh_;
  ros::ServiceServer navigation_srv;
  move_base_msgs::MoveBaseGoal goal;
  bool navigation_callback(cartesian_control::navigation_pose::Request &req, cartesian_control::navigation_pose::Response &res);

  void move_head(float position);
  void move_torso(float position);
  //void move_gripper(float position);


public:
  Navigation(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
  {
    navigation_srv = nh_.advertiseService("/cartesian_control/navigation", &Navigation::navigation_callback, this);
  }
  ~Navigation(){}
};


bool Navigation::navigation_callback(cartesian_control::navigation_pose::Request &req, cartesian_control::navigation_pose::Response &res)
{
    Navigation::move_head(0);
    Navigation::move_torso(0);
    ros::Duration(1).sleep();
    MoveBaseClient ac("move_base", true);
    while (!ac.waitForServer(ros::Duration(3.0))) {
      ROS_INFO("Waiting for the move_base action server to come up");
    }
    goal.target_pose.header.stamp =
    ros::Time::now();
    goal.target_pose.header.frame_id = req.frame;
    goal.target_pose.pose.position.x = req.posit_x;
    goal.target_pose.pose.position.y = req.posit_y;
    goal.target_pose.pose.orientation.z = req.orien_z;
    goal.target_pose.pose.orientation.w = req.orien_w;
    ac.sendGoal(goal);
    ac.waitForResult();
    if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
      res.done = true;
    }
    else{
      res.done = false;
    }
    return true;
}
void Navigation::move_head(float position)
{
    // Create an arm controller action client to move the TIAGo's arm
    joint_control_client ArmClient("/head_controller/follow_joint_trajectory",true);
    while(!ArmClient.waitForServer(ros::Duration(2.0)))
        ROS_INFO("waiting for the move_base action server to come up");

    control_msgs::FollowJointTrajectoryGoal head;
    // The joint names, which apply to all waypoints
    head.trajectory.joint_names.push_back("head_1_joint");
    head.trajectory.joint_names.push_back("head_2_joint");
    // Two waypoints in this goal trajectory
    head.trajectory.points.resize(1);
    // Positions
    head.trajectory.points[0].positions.resize(2);
    head.trajectory.points[0].positions[0] = 0;
    head.trajectory.points[0].positions[1] = position;
    // Velocities
    head.trajectory.points[0].velocities.resize(2);
    head.trajectory.points[0].velocities[0] = 0.0;
    head.trajectory.points[0].velocities[1] = 0.0;
    // To be reached 2 seconds after starting along the trajectory
    head.trajectory.points[0].time_from_start = ros::Duration(2.0);
    ArmClient.sendGoal(head);
    ArmClient.waitForResult();
}

void Navigation::move_torso(float position)
{
    // Create an arm controller action client to move the TIAGo's arm
    joint_control_client TorsoClient("/torso_controller/follow_joint_trajectory",true);
    while(!TorsoClient.waitForServer(ros::Duration(2.0)))
        ROS_INFO("waiting for the move_base action server to come up");

    control_msgs::FollowJointTrajectoryGoal torso;
    // The joint names, which apply to all waypoints
    torso.trajectory.joint_names.push_back("torso_lift_joint");
    // Two waypoints in this torso trajectory
    torso.trajectory.points.resize(1);
    // Positions
    torso.trajectory.points[0].positions.resize(1);
    torso.trajectory.points[0].positions[0] = position;
    torso.trajectory.points[0].velocities.resize(1);
    torso.trajectory.points[0].velocities[0] = 0.0;
    // To be reached 2 seconds after starting along the trajectory
    torso.trajectory.points[0].time_from_start = ros::Duration(2.0);
    TorsoClient.sendGoal(torso);
    TorsoClient.waitForResult();
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "navigation");
  ros::NodeHandle nh;
  Navigation node(nh);
  ros::spin();
  return 0;
}
